(ns reader.db.user
  (:require [clojure.java.jdbc :as jdbc]
            [reader.config :as config]
            [reader.db.utils :as u]))

(defn get-by-email
  ([email] (get-by-email email {:connection-uri (config/db-jdbc-uri)}))
  ([email connection-uri]
   (-> (jdbc/query connection-uri
                   ["select * from users where email=?" email])
       first
       u/record->map)))

(defn get-by-id
  ([id] (get-by-id id {:connection-uri (config/db-jdbc-uri)}))
  ([id connection-uri]
   (-> (jdbc/query connection-uri
                   ["select * from users where id=?" id])
       first
       u/record->map)))

(defn update-user
  ([user] (update-user user {:connection-uri (config/db-jdbc-uri)}))
  ([user connection-uri]
   (let [{:keys [email first-name last-name]} user]
     (jdbc/update!
      connection-uri
      :users
      {:first_name first-name :last_name last-name}
      ["email = ?" email]))))

(defn create-user
  ([user] (create-user user {:connection-uri (config/db-jdbc-uri)}))
  ([user connection-uri]
   (let [{:keys [email first-name last-name]} user]
     (if-let [existing-user (get-by-email email connection-uri)]
       (do (update-user user)
           (get-by-email email))
       (-> (jdbc/insert! connection-uri :users {:email email :first_name first-name :last_name last-name})
           first
           u/record->map)))))
