(ns reader.db.organization
  (:require [reader.config :as config]
            [clojure.java.jdbc :as jdbc]
            [reader.db.utils :as u]))

(defn get-by-slug
  ([slug] (get-by-slug slug {:connection-uri (config/db-jdbc-uri)}))
  ([slug connection-uri]
   (-> (jdbc/query connection-uri
                   ["select * from organizations where slug=?" slug])
       first
       u/record->map)))

(defn get-by-id
  ([id] (get-by-id id {:connection-uri (config/db-jdbc-uri)}))
  ([id connection-uri]
   (-> (jdbc/query connection-uri
                   ["select * from organizations where id=?" id])
       first
       u/record->map)))

(defn update-org
  ([org] (update-org org  {:connection-uri (config/db-jdbc-uri)}))
  ([org connection-uri]
   (let [{:keys [name slug]} org]
     (jdbc/update!
      connection-uri
      :organizations
      {:name name}
      ["slug = ?" slug]))))

(defn create-org
  ([org] (create-org org  {:connection-uri (config/db-jdbc-uri)}))
  ([org connection-uri]
   (let [{:keys [name slug]} org]
     (if-let [existing-org (get-by-slug slug)]
       (update-org org)
       (-> (jdbc/insert!
            connection-uri
            :organizations
            {:name name :slug slug})
           first
           u/record->map)))))
