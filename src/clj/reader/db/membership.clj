
(ns reader.db.membership
  (:require [clojure.java.jdbc :as jdbc]
            [reader.config :as config]
            [reader.db.utils :as u])
  (:import [org.postgresql.util PGobject]))

(defn cast-to-role-type [s]
  (doto (PGobject.)
    (.setType "role")
    (.setValue s)))

(defn get-member
  ([user-id org-id](get-member user-id org-id {:connection-uri (config/db-jdbc-uri)}))
  ([user-id org-id connection-uri]
   (-> (jdbc/query connection-uri
                   ["select * from members where user_id=? and org_id=?" user-id org-id])
       first
       u/record->map)))

(defn get-by-user-id
  ([user-id](get-by-user-id user-id {:connection-uri (config/db-jdbc-uri)}))
  ([user-id connection-uri]
   (map u/record->map (jdbc/query connection-uri
                                  ["select * from members where user_id=?" user-id]))))

(defn update-member
  ([member] (update-member member {:connection-uri (config/db-jdbc-uri)}))
  ([member connection-uri]
   (let [{:keys [user-id org-id role]} member]
     (jdbc/update!
      connection-uri
      :members
      {:role role}
      ["user_id=? and org_id=?" user-id org-id]))))

(defn create-member
  ([member] (create-member member {:connection-uri (config/db-jdbc-uri)}))
  ([member connection-uri]
   (let [{:keys [user-id org-id role]} member
         role-casted                   (cast-to-role-type role)
         member                        (assoc member :role role-casted)]
     (update-member member)
     (if-let [existing-member (get-member user-id org-id connection-uri)]
       (update-member member)
       (-> (jdbc/insert! connection-uri :members {:user_id user-id :org_id org-id :role role-casted})
           first
           u/record->map)))))
