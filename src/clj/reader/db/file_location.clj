(ns reader.db.file-location
  (:require [clojure.java.jdbc :as jdbc]
            [reader.config :as config]
            [reader.db.utils :as u]))

(defn get-file-locations-for-file-id
  ([file-id](get-file-locations-for-file-id file-id {:connection-uri (config/db-jdbc-uri)}))
  ([file-id connection-uri]
   (->> (jdbc/query
         connection-uri
         ["select type,location_url,version from file_locations where file_id = ?" file-id])
        (map u/record->map))))

(defn get-file-locations-for-file-id-type
  ([file-id file-type](get-file-locations-for-file-id-type file-id file-type {:connection-uri (config/db-jdbc-uri)}))
  ([file-id file-type connection-uri]
   (->> (jdbc/query
         connection-uri
         ["select location_url,version from file_locations where file_id = ? and type = ?" file-id file-type])
        (map u/record->map))))

(defn get-available-types-for-file
  ([file-id] (get-available-types-for-file file-id {:connection-uri (config/db-jdbc-uri)}))
  ([file-id connection-uri]
   (->> (jdbc/query
         connection-uri
         ["select type, location_url from file_locations where file_id = ?" file-id])
        (map u/record->map))))

(defn add-file-location
  ([data] (add-file-location data {:connection-uri (config/db-jdbc-uri)}))
  ([data connection-uri]
   (let [{:keys [file-id file-type location-url version]} data]
     (-> (jdbc/insert! connection-uri :file_locations
                       {:file_id      file-id
                        :type         file-type
                        :location_url location-url
                        :version      version})
         first))))
