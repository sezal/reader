(ns reader.db.directory
  (:require [clojure.java.jdbc :as jdbc]
            [cheshire.core :as json]
            [medley.core :as m]
            [camel-snake-kebab.core :as csk]
            [reader.config :as config]
            [reader.db.utils :as u]))

(extend-protocol clojure.java.jdbc/ISQLParameter
  clojure.lang.IPersistentVector
  (set-parameter [v ^java.sql.PreparedStatement stmt ^long i]
    (let [conn      (.getConnection stmt)
          meta      (.getParameterMetaData stmt)
          type-name (.getParameterTypeName meta i)]
      (if-let [elem-type (when (= (first type-name) \_) (apply str (rest type-name)))]
        (.setObject stmt i (.createArrayOf conn elem-type (to-array v)))
        (.setObject stmt i v)))))

(extend-protocol clojure.java.jdbc/IResultSetReadColumn
  java.sql.Array
  (result-set-read-column [val _ _]
    (into [] (.getArray val))))

(defn modify [tags]
  (let [s (json/encode tags)]
    (str "'{" (subs s 1 (dec (count s))) "}'")))

(defn get-by-org
  ([org-id] (get-by-org org-id {:connection-uri (config/db-jdbc-uri)}))
  ([org-id connection-uri]
   (-> (jdbc/query connection-uri
                   ["select * from directories where org_id = ? order by created_at desc" org-id]
                   {:row-fn u/record->map}))))

(defn get-by-org-tags
  ([org-id tags] (get-by-org-tags org-id tags {:connection-uri (config/db-jdbc-uri)}))
  ([org-id tags connection-uri]
   (-> (jdbc/query connection-uri
                   ["select * from directories where org_id=? and tags && ?" org-id "'{\"ad\",\"tag1\"}'"]
                   {:row-fn u/record->map}))))

(defn get-by-id
  ([id] (get-by-id id {:connection-uri (config/db-jdbc-uri)}))
  ([id connection-uri]
   (-> (jdbc/query
        connection-uri
        ["select * from directories where id = ?" id]
        {:row-fn u/record->map})
       first)))

(defn get-dir
  ([data] (get-dir data{:connection-uri (config/db-jdbc-uri)}))
  ([data connection-uri]
   (let [{:keys [name org-id parent-id]} data]
     (if parent-id
       (-> (jdbc/query
            connection-uri
            ["select * from directories where name= ? and org_id = ? and parent_id = ?" name org-id parent-id]
            {:row-fn u/record->map}))
       (-> (jdbc/query
            connection-uri
            ["select * from directories where name= ? and org_id = ? and parent_id is null" name org-id]
            {:row-fn u/record->map}))))))

(defn update-dir
  ([dir] (update-dir dir {:connection-uri (config/db-jdbc-uri)}))
  ([dir conneciton-uri] conneciton-uri))

(defn create-dir
  ([dir] (create-dir dir {:connection-uri (config/db-jdbc-uri)}))
  ([dir connection-uri]
   (let [{:keys [name parent-id org-id created-by-id tags]} dir]
     (if-let [abc false]
       (update-dir dir)
       (-> (jdbc/insert! connection-uri :directories
                         {:name          name
                          :parent_id     parent-id
                          :org_id        org-id
                          :created_by_id created-by-id
                          :tags          tags})
           first
           u/record->map)))))
