(ns reader.db.utils
  (:require [medley.core :as m]
            [camel-snake-kebab.core :as csk]))

(def ^:private ^:const keyword->column-name (fn [args] (csk/->snake_case_string args :separator \-)))
(def ^:private ^:const column-name->keyword (fn [args] (csk/->kebab-case-keyword args :separator \_)))

(defn record->map [record]
  (m/map-keys #(-> % name column-name->keyword) record))
