(ns reader.db.file
  (:require [clojure.java.jdbc :as jdbc]
            [reader.config :as config]
            [reader.db.utils :as u]))

(defn get-by-dir
  ([dir-id] (get-by-dir dir-id {:connection-uri (config/db-jdbc-uri)}))
  ([dir-id connection-uri]
   (->> (jdbc/query connection-uri
                    ["select * from files where location_directory = ?" dir-id])
        (map u/record->map))))

(defn get-by-id
  ([uuid] (get-by-id uuid {:connection-uri (config/db-jdbc-uri)}))
  ([uuid connection-uri]
   (-> (jdbc/query
        connection-uri
        ["select * from files where id=?" uuid]
        {:row-fn u/record->map})
       first)))

(defn update-file
  [file] )

(defn create-file
  ([file] (create-file file {:connection-uri (config/db-jdbc-uri)}))
  ([file connection-uri]
   (let [{:keys [id name dir-id]} file]
     (if-let [asdf false]
       (update-file file)
       (-> (jdbc/insert! connection-uri :files
                         {:id                 id
                          :name               name
                          :location_directory dir-id})
           first
           u/record->map)))))
