(ns reader.domain.user
  (:require [clj-http.client :as clj-http]
            [reader.redis :as redis]
            [reader.db.user :as user-db]))

(defn get-google-user [token]
  (let [google-response (clj-http/get
                         "https://www.googleapis.com/oauth2/v3/userinfo"
                         {:throw-exceptions false
                          :headers          {"Authorization" (if (not (clojure.string/starts-with? token "Bearer "))
                                                               (str "Bearer " token)
                                                               token)}
                          :as               :json})
        user-profile    (-> google-response :body)]
    user-profile))

(defn create-new-user [token]
  (let [user-info    (get-google-user token)
        db-user-info (clojure.set/rename-keys user-info {:name :first-name :given-name :last-name})
        user         (user-db/create-user db-user-info)
        user-id      (:id user)]
    (redis/add-token token user-id)))

(defn get-user [user-id]
  (user-db/get-by-id user-id))
