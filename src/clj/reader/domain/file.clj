(ns reader.domain.file
  (:require [clojure.java.io :as io]
            [medley.core :as m]
            [reader.db.file :as db]
            [reader.rabbitmq.publisher :refer [publish-message]]
            [reader.gcp.bucket-storage :refer [get-object-for-uri get-uri get-translated-file]]
            [reader.config :as config]
            [taoensso.timbre :as timbre]
            [ring.util.response :as res]))

(defn local-storage-loc [] (:local-file-storage config/config))

(defn save-to-disk
  "Given a file and a file name, saves the files to disk"
  [tempfile local-file]
  (io/copy tempfile (io/file local-file)))

(defn save-file [{:keys [filename] :as params}]
  (when (or (clojure.string/ends-with? filename ".pmd")
            (clojure.string/ends-with? filename ".p65"))
    (let [uuid           (m/random-uuid)
          extension      (if (clojure.string/ends-with? filename ".p65") ".p65" ".pmd")
          first-name     (-> filename
                             (clojure.string/replace (re-pattern extension) "")
                             (clojure.string/replace #" " "")
                             (clojure.string/replace #"\." "-"))
          name           first-name
          name-with-uuid (str name "-" uuid)
          location       (str (local-storage-loc) uuid extension)]
      (timbre/info "Extracting filename from" filename)
      (timbre/info "Saving file to " location)
      (save-to-disk (:tempfile params) location)
      (let [file (db/create-file {:id       uuid
                                  :name     name
                                  :dir-id   (:dir-id params)
                                  :file-url (get-uri uuid :pagemaker)})]
        (timbre/info "file created" file)
        (publish-message {:source-file-location location
                          :file-uuid            uuid
                          :file-type            (keyword (str "pagemaker-" extension))} "upload")
        (publish-message {:file-id       (:id file)
                          :file-location location} "convert")
        file))))

(defn list-files [dir]
  (db/get-by-dir (:dir-id dir)))

(defn get-english-translation [{:keys [id]}]
  (timbre/info "GET file uri" id)
  (assoc (get-translated-file id) :id id))

(defn initiate-translate [{:keys [id]}]
  (timbre/info "Received request: google translate file-id" id)
  (publish-message {:file-uuid id} "translate")
  (timbre/info "Initiate google translate file-id" id))
