(ns reader.domain.file-location
  (:require [reader.db.file-location :as db]
            [reader.gcp.bucket-storage :refer [get-object-for-uri get-translated-file]]
            [reader.rabbitmq.publisher :refer [publish-message]]
            [taoensso.timbre :as timbre]
            [reader.validate :as v]
            [schema.core :as s]))

(s/defschema AddFileLocation
  {:file-id      s/Uuid
   :file-type    s/Str
   :location-url s/Str})

(defn add-file-location
  [data]
  (db/add-file-location data))

(defn get-types-for-file
  [file-id]
  (db/get-available-types-for-file file-id))

(defn get-data-for-file-type
  [file-id file-type]
  (let [file-locations (db/get-file-locations-for-file-id file-id)
        location       (:location-url (first (filter #(= file-type (:type %)) file-locations)))]
    (timbre/info "Getting file" file-id " of type" file-type)
    (if (and (= "english-text" file-type) (nil? location))
      (let [index-uri (:location-url (first (filter #(= "index" (:type %)) file-locations)))
            file-data (get-translated-file index-uri)]
        (timbre/info "Getting translated file via index.csv")
        (def file-data* file-data)
        (if file-data
          (do (publish-message {:file-id      file-id
                                :file-type    "english-text"
                                :location-url (:location-url file-data)}
                               "add-location")
              (timbre/info "Translated file found")
              (assoc file-data :file-id file-id))
          (do
            (timbre/warn "File translation not found" file-id)
            {:text "File not translated yet"})))
      (assoc (get-object-for-uri location) :file-id file-id))))

(defn add-file-location-job [data]
  (let [validated-data (v/validate AddFileLocation data)]
    (add-file-location validated-data)))
