(ns reader.domain.organization
  (:require [reader.db.organization :as db]))

(defn get-org [org-id]
  (db/get-by-id org-id))

(defn create-org [{:keys [slug] :as org-data}]
  (let [does-slug-exist (db/get-by-slug slug)]
    (if does-slug-exist
      {:error :slug-already-exists}
      (db/create-org org-data))))

(defn find-or-create-org [org-data]
  (let [existing-org (db/get-by-slug (:slug org-data))]
    (if existing-org
      existing-org
      (db/create-org org-data))))
