(ns reader.domain.membership
  (:require [reader.db.membership :as db]))

;;add membership
(defn create-member [member]
  (db/create-member member))

;;is user member of an org
(defn is-member-of-org [user-id org-id]
  (> (count (db/get-member user-id org-id)) 0))

(defn get-membership-orgs [user-id]
  (db/get-by-user-id user-id))
