(ns reader.validate
  (:require [schema.core :as s]
            [schema.coerce :as coerce]
            [schema.utils :as s-utils]))

(def no-space (s/pred #(re-matches #"^[\S]+$" %)))

(def is-io-file (s/pred #(= java.io.File (type %))))

(defn coerce-and-validate [schema matcher data]
  (let [coercer (coerce/coercer schema matcher)
        result  (coercer data)]
    (if (s-utils/error? result)
      (throw (Exception. (format "Value does not match schema: %s"
                                 (s-utils/error-val result))))
      result)))

(defn validate [schema data]
  (coerce-and-validate schema coerce/json-coercion-matcher data))
