(ns reader.rabbitmq.consumer
  (:require [mount.core :as mount :refer [defstate]]
            [langohr.core :as rmq]
            [langohr.channel   :as lch]
            [langohr.exchange  :as le]
            [langohr.queue     :as lq]
            [langohr.consumers :as lc]
            [taoensso.timbre :as timbre]
            [reader.domain.file-location :refer [add-file-location-job]]
            [reader.rabbitmq.connection :refer [connection]]
            [reader.gcp.bucket-storage :refer [bucket-uploader-job]]
            [reader.convert.pm6 :refer [convert-job]]
            [reader.translate.google :refer [google-translate-job]]
            [cheshire.core :as json]
            [clojure.stacktrace :as st]))

(def ^{:const true}
  reader-exchange "reader")

(defn add-file-location-handler [ch meta payload]
  (let [data      (String. payload "UTF-8")
        json-data (json/parse-string data true)]
    (timbre/info "Picked up save file location job" json-data)
    (try
      (add-file-location-job json-data)
      (catch Exception e
        (st/print-stack-trace e)
        (timbre/error "Caught exception while saving file location to db" json-data e)))))

(defn upload-to-gcp-handler [ch meta payload]
  (let [data      (String. payload "UTF-8")
        json-data (json/parse-string data true)]
    (timbre/info "Picked up upload to GCP" json-data)
    (try
      (bucket-uploader-job json-data)
      (catch Exception e
        (st/print-stack-trace e)
        (timbre/error "Caught exception while uploading to gcp" json-data e)))))


(defn convert-pm6-to-text-handler [ch meta payload]
  (let [data      (String. payload "UTF-8")
        json-data (json/parse-string data true)]
    (timbre/info "Picked up convert pm6 file job" json-data)
    (try
      (convert-job json-data)
      (catch Exception e
        (timbre/error e)
        (timbre/error "Caught exception while converting pm6 object" json-data e)))))

(defn english-translate-handler [ch meta payload]
  (let [data      (String. payload "UTF-8")
        json-data (json/parse-string data true)]
    (timbre/info "Picked up english translation job" json-data)
    (try
      (google-translate-job json-data)
      (catch Exception e
        (timbre/error "Caught exception while translating hindi to english" json-data e)
        (st/print-stack-trace e)))))

(defn message-handler
  [ch {:keys [content-type delivery-tag type] :as meta} ^bytes payload]
  (println (format "[consumer] Received a message: %s, delivery tag: %d, content type: %s, type: %s"
                   (String. payload "UTF-8") delivery-tag content-type type)))


(defn- create-queues []
  (try (let [ch                 (lch/open connection)
             upload-queue       "gcp.upload"
             convert-queue      "text.convert"
             translate-queue    "text.translate"
             add-location-queue "file.addloc"]
         (lq/declare ch upload-queue { :auto-delete true})
         (lq/declare ch convert-queue { :auto-delete true})
         (lq/declare ch translate-queue { :auto-delete true})
         (lq/declare ch add-location-queue { :auto-delete true})
         (le/declare ch reader-exchange "direct" {:durable false :auto-delete true})
         (lq/bind ch upload-queue reader-exchange {:routing-key "upload"})
         (lq/bind ch convert-queue reader-exchange {:routing-key "convert"})
         (lq/bind ch translate-queue reader-exchange {:routing-key "translate"})
         (lq/bind ch add-location-queue reader-exchange {:routing-key "add-location"})
         (lc/subscribe ch upload-queue upload-to-gcp-handler {:auto-ack true})
         (lc/subscribe ch convert-queue convert-pm6-to-text-handler {:auto-ack true})
         (lc/subscribe ch translate-queue english-translate-handler {:auto-ack true})
         (lc/subscribe ch add-location-queue add-file-location-handler {:auto-ack true})
         (timbre/info "RabbitMQ queues created"))
       (catch Exception e
         (timbre/error e "Error while declaring RabbitMQ queues")
         (throw e))))

(defstate queues
  :start (create-queues))
