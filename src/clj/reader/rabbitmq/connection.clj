(ns reader.rabbitmq.connection
  (:require [mount.core :as mount :refer [defstate]]
            [langohr.core :as rmq]
            [taoensso.timbre :as timbre]))

(def ^{:const true}
  reader-exchange "reader")

(defn- start-connection []
  (timbre/info "Connecting to RabbitMQ")
  (try
    (rmq/connect)
    (catch Exception e
      (timbre/error e "Error while starting RabbitMQ connection")
      (throw e))))


(defn- close-connection [conn]
  (rmq/close conn)
  (timbre/info "Closing RabbitMQ connection"))


(defstate connection
  :start (start-connection)
  :stop (close-connection connection))
