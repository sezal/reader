(ns reader.rabbitmq.publisher
  (:require [reader.rabbitmq.connection :refer [connection reader-exchange]]
            [langohr.basic :as lb]
            [langohr.channel   :as lch]
            [taoensso.timbre :as timbre]

            [cheshire.core :as json]))

(defn publish-message
  "Publishes a weather update"
  [payload routing-key]
  (let [ch           (lch/open connection)
        json-payload (json/encode payload)]
    (lb/publish ch reader-exchange routing-key json-payload {:content-type "application/json"})
    (timbre/info "message published" json-payload routing-key)))
