(ns reader.core
  (:gen-class)
  (:require [mount.core :as mount]
            [taoensso.timbre :as timbre]
            [reader.config :as config]
            [reader.db :as db]
            [reader.gcp.bucket-storage :as bucket]
            [reader.rabbitmq.connection :as connection]
            [reader.rabbitmq.consumer :as consumer]
            [reader.db-migration :as db-migration]
            [reader.server :as server]))

(defn- start-api []
  (try
    (mount/stop)
    (timbre/info "Starting Pagemaker Reader")
    (mount/start #'config/config)
    (db-migration/migrate)
    (mount/start)
    (timbre/info "Started api service")
    (catch Exception e
      (timbre/error e "Error in starting api service")
      (mount/stop)
      (System/exit 1))))

(defn- run-job [job-name job-fn]
  (try
    (job-fn)
    (System/exit 0)
    (catch Exception e
      (timbre/error e "Error while running job")
      (System/exit 1))))

(defn- run-migrate []
  (mount/start #'config/config)
  (db-migration/migrate))

(defn- run-rollback []
  (mount/start #'config/config)
  (db-migration/rollback))

(defn -main [& [command]]
  (case command
    ;;  "api"      (start-api)
    "migrate"  (run-job "migrate" run-migrate)
    "rollback" (run-job "rollback" run-rollback)
    (start-api)
    #_         (do
                 (print "Must supply a valid command to run")
                 (System/exit 1))))

(defn restart-server []
  (mount/stop)
  (mount/start))
