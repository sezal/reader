(ns reader.server
  (:require [mount.core :refer [defstate]]
            [taoensso.timbre :as timbre]
            [clojure.stacktrace :as st]
            [ring.adapter.jetty :as ring-jetty]
            [ring.middleware.session :refer [wrap-session]]
            [ring.middleware.oauth2 :refer [wrap-oauth2]]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
            [ring.middleware.multipart-params :refer [wrap-multipart-params]]
            [ring.util.response :as res]
            [bidi.ring :refer [make-handler]]
            [reader.config :refer [config]]
            [reader.routes :refer [authorized-routes]]
            [reader.redis :as redis]
            [reader.domain.user :as user])
  (:import (org.eclipse.jetty.server Server)))



(def reloadable-routes (wrap-reload #'authorized-routes))

(defn wrap-login [handler]
  (fn [req]
    (if-let [token (-> req :session :ring.middleware.oauth2/access-tokens :google :token)]
      (if-let [user-id (redis/get-user-from-token token)]
        (handler (assoc req :user-id user-id))
        (handler (assoc req :user-id (user/create-new-user token))))
      (res/redirect "/login"))))

(defn wrap-request-log [handler]
  (fn [req]
    (timbre/info "Received request" (:uri req))
    (handler req)))

(defn wrap-exception-handling
  "Catches an exception and returns the appropriate HTTP response."
  [handler-fn]
  (fn [request]
    (try (handler-fn request)
         (catch Exception e
           (timbre/error "Uncaught exception: " {:request    (prn-str request)
                                                 :stacktrace (with-out-str
                                                               (st/print-stack-trace e))})
           {:status 500
            :body   "Internal Server error"}))))

(defn handler []
  (-> authorized-routes
      make-handler
      wrap-login
      wrap-request-log
      wrap-exception-handling
      wrap-json-response
      (wrap-oauth2 (:oauth2-spec config))
      (wrap-json-body {:keywords? true})
      wrap-keyword-params
      wrap-params
      wrap-multipart-params
      (wrap-session)))

(def app (handler))

(defn- start [handler]
  (let [conf (:http-server config)
        port (:port conf)]
    (timbre/info "Starting server on port:" port)
    (ring-jetty/run-jetty (handler) conf)
    #_(ring-jetty/run-jetty (wrap-reload #'app) conf)))

(defn- stop [server]
  (timbre/info "Stopping server")
  (.stop server))

(defstate server
  :start (start handler)
  :stop (stop server))
