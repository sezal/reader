(ns reader.gcp.utils
  (:require [reader.config :refer [config]]))

(defn get-uri [uuid type]
  (let [bucket-url (-> config :google :bucket-uri)]
    (str bucket-url uuid "/" (get-in config [:file-names type]))))
