(ns reader.gcp.bucket-storage
  (:require [clj-gcloud.storage :as st]
            [mount.core :refer [defstate] :as mount]
            [taoensso.timbre :as timbre]
            [reader.config :refer [config]]
            [clojure.java.io :as io]
            [schema.core :as s]
            [reader.validate :as v]
            [reader.rabbitmq.publisher :refer [publish-message]]))

(s/defschema GCPUploadData
  {:source-file-location s/Str
   :file-uuid            s/Uuid
   :file-type            s/Keyword})

(defn get-uri [uuid type]
  (let [bucket-url (-> config :google :bucket-uri)]
    (str bucket-url uuid "/" (get-in config [:file-names type]))))

(defstate bucket-client
  :start (do
           (timbre/info "Starting GCP bucket client")
           (st/init {:project-id  "lakshya-reader"
                     :credentials (:credentials (:google config))}))
  :stop (timbre/info "Stopping bucket storage client"))

(defn bucket-url []
  (-> config :google :bucket-uri))

(defn upload-object [file-location file-name]
  (st/copy-file-to-storage
   bucket-client
   (io/file file-location)
   file-name))

(defn get-object-for-uri [gs-uri]
  (let [temp-file      (java.io.File/createTempFile "temp" ".txt")
        temp-file-path (.getAbsolutePath temp-file)]
    (try
      (st/download-file-from-storage bucket-client gs-uri temp-file-path)
      {:text (slurp temp-file-path)}
      (catch java.lang.NullPointerException e
        {:text "File not found"}))))

(defn view-objects [at-location]
  (st/ls bucket-client (bucket-url)))

(defn bucket-uploader-job [data]
  (let [validated-data                (v/validate GCPUploadData data)
        {:keys [source-file-location
                file-uuid file-type]} validated-data
        file-name                     (get-uri file-uuid file-type)]
    (publish-message {:file-id      file-uuid
                      :file-type    file-type
                      :location-url (get-uri file-uuid file-type)}
                     "add-location")
    (upload-object source-file-location file-name)
    (timbre/info "Upload finished to GCP" validated-data)))

(defn get-translated-file-location [index-uri]
  (let [index (:text (get-object-for-uri index-uri))]
    (def index* index)
    (when (not= index "File not found")
      (-> index
          (clojure.string/split #",")
          (nth 2)))))

(defn get-translated-file [index-uri]
  ;;index file not found
  ;; index file found but translated file not found
  (let [translate-file-loc (get-translated-file-location index-uri)]
    (when translate-file-loc
      (assoc
       (get-object-for-uri translate-file-loc)
       :location-url translate-file-loc))))
