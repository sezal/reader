(ns reader.routes
  (:require [bidi.ring :as bidi]
            [ring.util.response :as res]
            [reader.handler.user :as users]
            [reader.handler.organization :as org]
            [reader.handler.membership :as member]
            [reader.handler.directory :as dir]
            [reader.handler.file :as file]
            [reader.handler.file-location :as file-location]
            [clojure.java.io :as io]
            [taoensso.timbre :as timbre]))

(defn ping [req]
  (timbre/info req)
  (res/response "pong"))

(defn home [req] (res/response (io/input-stream (io/resource "public/index.html"))))

(def org-routes {"" {:get member/get-orgs}
                 ["/" :org-id "/dirs/"]
                 {""                  {:get  dir/list
                                       :post dir/create}
                  [:dir-id "/files/"] {""         {:get file/list}
                                       [:file-id] {"/get-types"          {:get file-location/get-types}
                                                   "/file-data-for-type" {:get file-location/get-data}
                                                   "/translate-file"     {:post file/initiate-translate}}}}})

(def authorized-routes ["" {""  home
                            "/" {""            home
                                 "ping"        ping
                                 "user"        {:get users/create-user}
                                 "user-info"   {:get users/get-user-info}
                                 "orgs"        org-routes
                                 "js"          (bidi/resources {:prefix "public/js/"})
                                 "css"         (bidi/resources {:prefix "public/css/"})
                                 "fonts"       (bidi/resources {:prefix "public/fonts/"})
                                 "images"      (bidi/resources {:prefix "public/images/"})
                                 "file-upload" file/upload}}])


;;directory routes --> get directories with parent id, org id, show tree
;;create org
;;create a member --> user-id, org-id
