(ns reader.handler.directory
  (:require   [reader.domain.directory :as directory]
              [reader.validate :as v]
              [ring.util.response :as res]
              [schema.core :as s]))

(def no-space (s/pred #(re-matches #"^[\S]+$" %)))

(s/defschema ListDirectoryRequest
  {:org-id                     s/Uuid
   (s/optional-key :parent-id) (s/maybe s/Uuid)
   :show-tree                  s/Bool
   s/Keyword                   s/Any})

(s/defschema CreateDirectoryRequest
  {:org-id                     s/Uuid
   (s/optional-key :parent-id) (s/maybe s/Uuid)
   :created-by-id              s/Uuid
   :name                       no-space
   (s/optional-key :tags)      (s/maybe [s/Str])
   s/Keyword                   s/Any})

(defn create [{:keys [params user-id body] :as request}]
  "create a directory for an org, with/without a parent directory"
  (let [dir-data (assoc
                  (select-keys body [:parent-id :name :tags])
                  :created-by-id user-id
                  :org-id (:org-id params))
        dir      (directory/create (v/validate CreateDirectoryRequest dir-data))]
    (condp #(contains? %2 %1) dir
      :error (-> (res/response (str (:error dir)))
                 (res/status 409))
      :id    (-> (res/response dir)
                 (res/status 200)))))

(defn list [{:keys [params]}]
  "get directories based on org param"
  (let [filter-by (select-keys params [:org-id :show-tree :parent-id])]
    (res/response
     (directory/list
      (v/validate ListDirectoryRequest filter-by)))))

#_(defn find [request org-id id]
    "get full details of a single directory with id"
    (let [params {:org-id org-id :id id}]
      (res/response (directory/get-one-item params))))
