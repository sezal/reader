(ns reader.handler.file-location
  (:require [schema.core :as s]
            [reader.domain.file-location :as file-loc]
            [reader.validate :as v]
            [ring.util.response :as res]))

(s/defschema GetFileTypesRequest
  {:file-id s/Uuid})

(s/defschema GetFileDataRequest
  {:file-id   s/Uuid
   :file-type s/Str})

(defn get-types
  [{:keys [params]}]
  (let [data (v/validate GetFileTypesRequest {:file-id (:file-id params)})]
    (res/response (file-loc/get-types-for-file (:file-id data)))))

(defn get-data
  [{:keys [params]}]
  (let [data (v/validate GetFileDataRequest (select-keys params [:file-id :file-type]))]
    (res/response (file-loc/get-data-for-file-type (:file-id data) (:file-type data)))))
