(ns reader.handler.membership
  (:require [reader.domain.membership :as member]
            [reader.domain.organization :as org]
            [ring.util.response :as res]))

(defn get-orgs [{:keys [user-id]}]
  (let [org-ids (map :org-id (member/get-membership-orgs user-id))
        orgs    (map org/get-org org-ids)]
    (res/response orgs)))
