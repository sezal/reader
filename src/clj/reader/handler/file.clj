(ns reader.handler.file
  (:require [ring.util.response :as res]
            [schema.core :as s]
            [reader.domain.file :as file]
            [reader.validate :as v]
            [taoensso.timbre :as timbre]))

(s/defschema ListFileRequest
  {:dir-id s/Uuid})

(s/defschema UploadFileRequest
  {:filename s/Str ;;v/no-space
   :tempfile v/is-io-file
   :dir-id   s/Uuid})

(s/defschema GetFileRequest
  {:id s/Uuid})

(s/defschema GetEnglishFileRequest
  {:id s/Uuid})

(s/defschema TranslateFileRequest
  {:id s/Uuid})

(defn list [{:keys [params]}]
  (let [filter-by (select-keys params [:dir-id])]
    (res/response
     (file/list-files
      (v/validate ListFileRequest filter-by)))))

(defn upload [req]
  (let [filename  (get-in req [:params :file-input :filename])
        tempfile  (get-in req [:params :file-input :tempfile])
        directory (get-in req [:params :directory])]
    (file/save-file (v/validate UploadFileRequest {:filename filename
                                                   :tempfile tempfile
                                                   :dir-id   directory}))
    (res/response {:success true})))

(defn get-english-file [{:keys [params]}]
  (let [file-id   (:file-id params)
        file-data (file/get-english-translation (v/validate GetEnglishFileRequest {:id file-id}))]
    (timbre/info "Getting translation for file" file-id)
    (res/response file-data)))

(defn initiate-translate [{:keys [params]}]
  ;;defaulting to english translation
  (let [file-id (:file-id params)]
    (file/initiate-translate (v/validate TranslateFileRequest {:id file-id}))
    (res/response "Translation Requested")))
