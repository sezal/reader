(ns reader.handler.user
  (:require [ring.util.response :as res]
            [reader.redis :as redis]
            [reader.domain.user :as user]))

(defn create-user [request]
  (if-let [token (-> request :oauth2/access-tokens :google :token)]
    (if (redis/get-user-from-token token)
      (res/redirect "/")
      (do (user/create-new-user token)
          (res/redirect "/")))
    (res/redirect "/login")))

(defn get-user-info [{:keys [user-id]}]
  (res/status (res/response (user/get-user user-id)) 200))
