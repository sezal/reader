(ns reader.handler.organization
  (:require [reader.db.organization :as db]
            [reader.domain.organization :as domain]
            [reader.domain.membership :as member]
            [ring.util.response :as res]))

(defn create
  "Create an organization and make the creator an admin"
  [{:keys [user-id]} body]
  (let [org (domain/create-org body)]
    (if (contains? org :id)
      (do
        (member/create-member {:user-id user-id
                               :org-id  (:id org)
                               :role    "admin"})
        (-> (res/response org)
            (res/status 200)))
      (-> (res/response (str (:error org)))
          (res/status 409)))))
