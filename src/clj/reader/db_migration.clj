(ns reader.db-migration
  (:require [ragtime.repl :as ragtime]
            [ragtime.jdbc :as jdbc]
            [taoensso.timbre :as timbre]
            [reader.config :refer [db-jdbc-uri]]))


(defn- migration-config []
  {:datastore  (jdbc/sql-database {:connection-uri (db-jdbc-uri)})
   :migrations (jdbc/load-resources "migrations")
   :reporter   (fn [_ op id]
                 (case op
                   :up   (timbre/info "Applying migration" id)
                   :down (timbre/info "Rolling back migration" id)))})

(defn migrate []
  (ragtime/migrate (migration-config))
  (timbre/info "Ran all migrations"))

(defn rollback []
  (ragtime/rollback (update (migration-config) :migrations #(vector (last %)))))
