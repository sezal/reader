(ns reader.config
  (:require [mount.core :refer [defstate]]
            [aero.core :as aero]
            [taoensso.timbre :as timbre]))


(defmethod aero/reader 'from-env
  [opts tag [env-key default-value]]
  (if-let [value-from-env (System/getenv (str env-key))]
    value-from-env
    default-value))

(defn make-config [config-file]
  (let [config (-> config-file
                   (clojure.java.io/resource)
                   (aero/read-config))]
    (timbre/info "Loaded config from file:" config-file)
    (timbre/debug "Config:\n" (with-out-str (-> config (clojure.pprint/pprint))))
    config))

(defstate config
  :start (make-config "config.edn")
  :stop (timbre/info "Stopped config"))

(defn db-jdbc-uri []
  (let [{:keys [server-name port database-name username password adapter]} (:db config)]
    (format "jdbc:%s://%s:%s/%s?user=%s&password=%s"
            adapter server-name port database-name username password)))
