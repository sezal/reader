(ns reader.convert.question
  (:require [clojure.string :as s]))

;;find questions in a certain txt
;; and tag them accordingly
(def question-separator "◦")
(def option-separator "\\(\\d\\)")
(def separators ["◦" "\\(\\d\\)" "\\d\\."])
(def options ["(1)" "(2)" "(3)" "(4)"])

(defn full-separator []
  (s/join "|" (map #(str "(?<=" % ")|(?=" % ")") separators)))

(defn full-question-separator []
  (re-pattern (str "(?<=" option-separator
                   ")|(?=" option-separator
                   ")|(?<=" question-separator
                   ")|(?=" question-separator ")")))

(defn add-tag [txt tag]
  (let [tag-close (s/replace tag #"<" "</")]
    (str tag txt tag-close)))


#_(defn question-markdown [ques]
    (str (:question-text ques)
         (add-tag
          (s/join (mapv #(add-tag % "<li>") (vals (:options ques))))
          "<ol>")
         (when (:answer ques) (str "\n Answer: " (:answer ques)))
         (when (:hint ques) (str "\n " (:hint ques)))
         "\n\n ---\n"))

#_(defn question-json [ques]
    {:question-text (nth ques 0)
     :options       {1 (nth ques 2)
                     2 (nth ques 4)
                     3 (nth ques 6)
                     4 (nth ques 8)}
     :answer        (Integer/parseInt (re-find #"\d"(nth ques 9)))
     :hint          (nth ques 10 nil)})

(defn is-valid-question? [ques]
  (if (< (count ques) 9)
    false
    (every? identity (map #(> (.indexOf ques %) -1) options))))

(defn grouped-questions-valid? [text-array]
  (reduce
   (fn [acc v]
     ;;     (prn "group questions valid" (is-valid-question? v))
     (if (vector? v)
       (if (is-valid-question? v)
         (do
           (prn "Question: " v)
           (conj acc v))
         (conj acc (s/join " " v)))
       (conj acc v)))
   []
   text-array))

(defn group-question [text-array]
  (reduce
   (fn [acc v]
     (let [q-start      (= v question-separator)
           last-ques    (last acc)
           last-q-index (dec (count acc))]
       (if q-start
         (merge acc [v])
         (if (vector? last-ques)
           (assoc acc last-q-index (merge last-ques v))
           (conj acc v)))))
   []
   text-array))

(defn question-markdown [ques]
  (prn (map-indexed vector ques))
  (let [ques-options         (filter
                              #(re-matches (re-pattern option-separator) (second %))
                              (map-indexed vector ques))
        correct-option       (last (remove nil? ques-options))
        correct-option-index (first correct-option)
        updated-ques         (concat (subvec ques 0 correct-option-index)
                                     (subvec ques (inc correct-option-index)))
        markdown-tags        (assoc {question-separator "!Q"
                                     "(1)"              "!A1"
                                     "(2)"              "!A2"
                                     "(3)"              "!A3"
                                     "(4)"              "!A4"}
                                    (second correct-option)
                                    "!A0")]
    (s/join
     "\n"
     (map #(if (markdown-tags %) (markdown-tags %) %) updated-ques))))

(defn add-question-markdown [text-array]
  (reduce
   (fn [acc v]
     (if (vector? v)
       (conj acc (question-markdown v))
       (conj acc v)))
   []
   text-array))

(defn parse-questions [text]
  ;;(prn text)
  (prn "parsing questions")
  (->> (-> (str text)
           (s/split (full-question-separator)))
       ;;  (map  #(s/trim-newline (s/trim %)))
       (remove empty?)
       group-question
       grouped-questions-valid?
       add-question-markdown
       (s/join "\n\n ")))


#_(def s " सेवा का अनुभव होना चाहिए।\r\n  \n  ◦ राजस्थान लोक सेवा आयोग का सचिव होता है-\n  \n  \t(1)\tभारतीय प्रशासनिक सेवा का अधिकारी\n  \t(2)\tराजस्थान प्रशासनिक सेवा का अधिकारी\n  \t(3)\tराज्य सचिवालयी सेवा का अधिकारी\n  \t(4)\tभारतीय पुलिस सेवा का अधिकारी\t(1)\n  \n  \n  ◦ राजस्थान लोक सेवा आयोग की भूमिका राज्य सरकार के लिए होती है-\n  \n  \t(1)\tसलाहकारी\t(2)\tबाध्यकारी\n  \t(3)\tकानूनी\t(4)\tउक्त कोई नहीं\t(1)\n  **व्याख्या** -यदि राज्य सरकार आयोग की सलाह नहीं मानती है तो उसे केवल विधानसभा में इन सुझावों को न मानने के कारण स्पष्ट करने होते हैं।\n  \n  \n  ◦ राजस्थान लोक सेवा आयोग में सदस्य बनने हेतु आवश्यक योग्यता है-\n  \n  \t(1)\tअधिस्नातक स्तर तक शिक्षा प्राप्त हो\n  \t(2)\tराज्यपाल की राय में विषय-विशेषज्ञ हो।\n  \t(3)\tराजकीय सेवा में राजपत्रित पद पर कार्य कर चुका हो।\n  \t(4)\tउक्त कोई नहीं\t\t\t(4)\n  **व्याख्या** -लोक सेवा आयोग के सदस्य होने के लिए संविधान में कोई योग्यता निर्धारित नहीं की गई है, परन्तु कुल सदस्यों के कम से कम आधे सदस्यों को केन्द्र या राज्य सरकार में कम से कम 10 वर्ष की सेवा का अनुभव होना चाहिए।\n  \n  \n  ◦ राजस्थान लोक सेवा आयोग के जुलाई, 2017 में नियुक्त किये गये अध्यक्ष हैं-\n  \n  \t(1)\tसर एस.सी. त्रिपाठी\t\n  \t(2)\tसर एस. के. घोष\n  \t(3)\tश्री श्याम सुंदर शर्मा\t\n  \t(4)\tश्री देवीशंकर त्रिपाठी\t\t(3)\n  \r\n  \n  ◦ देश में संघ लोक सेवा आयेाग के गठन की सर्वप्रथम किस समिति ने अनुशंषा की थी?\n  \n  \t(1)\tली कमीशन\t\n  \t(2)\tलोक सेवा समिति\n  \t(3)\tप्रशासनिक सुधार आयोग\n  \t(4)\tसिविल सर्विसेज प्रशासन समिति\t(1)")
