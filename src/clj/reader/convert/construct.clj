(ns reader.convert.construct
  (:require [reader.convert.chanakya :refer [chanakya-to-unicode]]
            [reader.convert.kundli :refer [kundli-to-unicode]]
            [reader.convert.symbols :refer [add-symbols]]
            [clojure.string :as s]))

;; takes in parsed records and constructs a markdown formatted text out of it.

(defn debug [x] (prn x) x)

(def use-markdown false)

(defn filter-by-rec-type [record-data rec-type]
  (filter
   #(= rec-type
       ((if (keyword? rec-type) :rec-type-name :rec-type) %))
   record-data))

(defn filter-by-seq-num [record-data seq-num]
  (filter
   #(contains?
     (set (if seq? seq-num [seq-num]))
     (:seq %))
   record-data))

(defn remove-extra-chars [text extra-chars]
  (if (empty? extra-chars)
    text
    (remove-extra-chars
     (s/replace text (re-pattern (first extra-chars)) "")
     (rest extra-chars))))

(defn add-tag
  ([text tag props]
   (let [tag-start (s/replace tag #">" (s/join [(reduce (fn [acc [k v]] (str " " k "=" v " ")) "" props) ">"]))
         tag-close (s/replace tag #"<" "</")]
     (if use-markdown
       (str tag-start text tag-close)
       text)))
  ([text tag]
   (let [tag-close (s/replace tag #"<" "</")]
     (if use-markdown
       (str tag text tag-close)
       text))))

(defn add-tag- [txt tag]
  (if (= "-" (s/trim txt))
    txt
    (str tag (s/trim txt) tag " ")))

(defn add-para [txt]
  (if use-markdown
    (add-tag txt "<p>")
    #_(str txt "<br>")
    (str "\n " txt)))

(defn add-heading [txt font-size font]
  (cond
    (and (= font "Chanakya") (= 220 font-size))        (add-para txt)
    (and (= font "Times New Roman") (= 180 font-size)) (add-para txt)
    (and (= font "Chanakya") (= 180 font-size))        (add-para txt)
    (and (= font "Chanakya") (= 160 font-size))        (add-para txt)
    :else                                              txt))

(defn markdown [txt {:keys [font font-face font-color font-size bold italic sub super underline]}]
  (if (= 0 (count (s/trim txt)))
    txt
    (cond-> txt
      (= "Chanakya" font)    (chanakya-to-unicode)
      (= "Kundli" font)      (kundli-to-unicode)
      (= "Wingdings 2" font) (add-symbols)
      ;; (= 1 bold)             (add-tag- "**")
      ;; (= 1 italic)           (add-tag- "_")
      ;; (= 1 sub)              (add-tag "<sub>")
      ;; (= 1 super)            (add-tag "<sup>")
      ;; (= 1 underline)        (add-tag "<u>")
      ;; (= 1 bold)      (add-tag "<strong>")
      ;; (= 1 italic)    (add-tag "<em>")
      ;; (= 1 sub)       (add-tag "<sub>")
      ;; (= 1 super)     (add-tag "<sup>")
      ;; (= 1 underline) (add-tag "<u>")
      font-size              (add-heading font-size font)
      #_                     (add-tag "<font>" {"size" (Math/floor (/ font-size 25))})
      :always                (remove-extra-chars ["" "" "" "" "" "" ""
                                                  "\\*\\*\\*\\*" "\\*\\* \\*\\*" ]))))

#_(defn add-markdown-table
    ([txt] (add-markdown-table txt "="))
    ([txt column-separator]
     (if (> (count (re-seq (re-pattern column-separator) txt)) 1)
       (s/replace txt (re-pattern column-separator) "|")
       txt)))

(defn add-markdown-table [txt]
  (s/join "|" (map s/trim (s/split txt  #"="))))

(defn para-format [txt {:keys [align left-indent after-indent before-indent right-indent first-indent rule-below rule-above orphans widows keep-with-next hyphen-count hyphenate length] :as v}]
  (if (= 0 (count txt))
    txt
    (cond-> txt
      #_      (> (count (re-seq #"=" txt)) 1) #_ (add-markdown-table)
      #_      (= 2 align)                     #_ (add-tag "<center>")
      :always (add-para))))


(defn construct-text-data [text-related-records]
  (let [grouped-records (->> text-related-records
                             (map #(select-keys % [:seq :rec-type-name :parsed-data]))
                             (group-by :rec-type-name))]
    (reduce
     (fn [acc [k v]]
       (let [grouped-data (map :parsed-data v)]
         (assoc
          acc k
          (if (= :txt k)
            (clojure.string/join grouped-data)
            (flatten grouped-data)))))
     {} grouped-records)))

(defn apply-char-props [txt char-props-list offset]
  ;;apply char props to a signle para text
  ;;we have txt distributed into paras at this point
  ;; every para has start index and sub string
  (reduce
   ;;this reduce R1 loops over relevant chars and applies markdown based on char props to txt
   (fn [acc v]
     (let [start (- (max (:start v) offset) offset)
           end   (- (min (:end v) (+ offset (count txt))) offset)]
       (clojure.string/join [acc (markdown (subs txt start end) v)])))
   ""
   char-props-list))

(defn apply-para-char-props [txt para-props-list char-props-list]
  (reduce
   (fn [acc para]
     (let [str-len        (count txt)
           end            (min (or (:end para) str-len) str-len)
           start          (min (or (:start para) 0) str-len)
           relevant-chars (filter #(and (< (:start %) end) (> (:end %) start)) char-props-list)]
       (when (and start end)
         (merge acc {:txt (-> (subs txt start end)
                              (apply-char-props relevant-chars start)
                              (para-format para))}))))
   []
   para-props-list))


(defn remove-duplicates [prop-list]
  (reduce
   (fn [acc v]
     (let [last-v  (last acc)
           is-same (= (dissoc last-v :length) (dissoc v :length))]
       #_(if is-same (prn "found same " v)
             (prn last-v v))
       (if is-same
         (assoc acc (dec (count acc)) (assoc last-v :length (+ (:length last-v) (:length v))))
         (merge acc v))))
   []
   prop-list))

(defn add-start-end [prop-list]
  (reductions
   (fn [acc v]
     (let [start (or (:end acc) (:length acc))]
       (assoc v :start start :end (+ start (:length v)))))
   (assoc (first prop-list) :start 0 :end (:length (first prop-list)))
   (rest prop-list)))

(defn construct-html-text [data fonts text-related-records]
  (let [records                  (filter-by-seq-num data text-related-records)
        {:keys [txt para chars]} (construct-text-data records)
        txt                      (or txt "")]
    (if (and chars (> (count txt) 0))
      (let [chars-with-start-end (map #(assoc % :font (nth fonts (:font-face %))) (add-start-end chars))
            para-with-start-end  (add-start-end (filter  #(<= (:length %) (count txt)) para))
            transformed-data     (apply-para-char-props  txt  para-with-start-end chars-with-start-end)]
        {:formatted-text (clojure.string/join (map :txt transformed-data))
         :txt            txt})
      {:formatted-text ""
       :txt            ""})))

#_(defn construct-html-text [data fonts text-related-records]
    (let [records                  (filter-by-seq-num data text-related-records)
          {:keys [txt para chars]} (construct-text-data records)
          txt                      (or txt "")
          __                       (prn "count "(count chars)  chars "txt" txt)
          chars-with-start-end     (map #(assoc % :font (nth fonts (:font-face %))) (add-start-end chars))
          para-with-start-end      (add-start-end para)
          _                        (prn (count chars-with-start-end) (count para-with-start-end))
          transformed-data         (apply-para-char-props  txt  para-with-start-end chars-with-start-end)]
      ;;    (prn transformed-data "=====================")
      {:formatted-text (clojure.string/join (map :txt transformed-data))
       :txt            txt}))

(defn construct-text-block [data fonts parsed-data]
  (if (seq? parsed-data)
    (map #(construct-text-block data fonts %) parsed-data)
    (construct-html-text data fonts (:related-records parsed-data))))

(defn construct-document [data]
  (let [colors      (map :parsed-data (filter-by-rec-type data :colors))
        fonts       (flatten (map :parsed-data (filter-by-rec-type data :fonts)))
        global-info (map :parsed-data (filter-by-rec-type data :global-info))
        pages       (map :parsed-data (filter-by-rec-type data :page))
        text-block  (map :parsed-data (filter-by-rec-type data :text-block))]
    (map #(construct-text-block data fonts %)  (first text-block))))
