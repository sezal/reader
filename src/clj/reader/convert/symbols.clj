(ns reader.convert.symbols
  (:require [clojure.string :as s]))


(def symbols [["²" "\n• "]
              ["¶" "\n  ◦ "]
              ["" "‣"]
              ["\t" ""]
              ["\r" "\n  "]
              ["P" "✓"]])

(defn add-symbols
  ([text symbols-key]
   (if (empty? symbols-key)
     text
     (let [k (first symbols-key)]
       (add-symbols
        (s/replace text (re-pattern (first k)) (last k))
        (rest symbols-key)))))
  ([text]   (add-symbols text symbols)))
