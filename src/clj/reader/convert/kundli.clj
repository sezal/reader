(ns reader.convert.kundli
  (:require [clojure.string :as s]
            [reader.convert.font-utils :as u]))

(def kundli-key
  {:double-letter-keys [["kW" "ॉ"]
                        ["\\{k" "क्ष"]
                        ["\\)" "द्ध"]
                        ["\\|" "द्य"]]
   :single-letter-keys [["\\{" "्"] ["~" "्"]
                        ["k" "ा"]
                        ["q" "ु"] ["w" "ू"] ["`" "ृ"]
                        ["s" "े"] ["S" "ै"]
                        ["f" "ि"] ["h" "ी"]
                        ["\\+" "़"]
                        ["a" "ं"] [" " "ँ"]
                        ["z" "्र"] ["Z" "र्"] ["©" "्र"] [":" "रू"] ["#" "रु"]
                        ["v" "अ"]
                        ["b" "इ"]
                        ["_" "ऋ"]
                        ["," "ए"]
                        ["m" "उ"] ["Ä" "ऊ"]
                        ["d" "क"] ["D" "क्"] ["\\[" "ख्"] ["x" "ग"] ["\\?" "घ्"]
                        ["p" "च"] ["N" "छ"] ["t" "ज"] ["T" "ज्"] [">" "झ"]
                        ["V" "ट"] ["B" "ठ"] ["M" "ड"] ["<" "ढ"] ["\\." "ण्"]
                        ["r" "त"] ["R" "त्"] ["f" "थ"] ["F" "थ्"] ["n" "द"] ["/" "ध्"] ["u" "न"] ["U" "न्"]
                        ["i" "प"] ["I" "प्"] ["Q" "फ"] ["c" "ब"] ["C" "ब्"] ["H" "भ्"] ["e" "म"] ["E" "म्"]
                        [";" "य"] ["j" "र"] ["y" "ल"] ["Y" "ल्"] ["o" "व"] ["O" "व्"]
                        ["'" "श्"] ["\"" "ष्"] ["l" "स"] ["L" "स्"] ["g" "ह"]
                        ["=" "त्र"] ["K" "ज्ञ"]
                        ["×" "क्र"] ["Ø"  "त्त्"] ["}" "द्व"] ["J" "श्र"]]
   :cleanup            [["्ा" ""]
                        ["अाै" "औ"]
                        ["आे"  "ओ"]
                        ["अा" "आ"]
                        ["र्इ" "ई"] ;;because of the mistakes of typists
                        ["एे" "ऐ"]]
   :symbols            [["A" "।"]
                        ["%" ":"]
                        ;;                        ["\\" "?"]
                        ["&" "—"]
                        ["»" "("]
                        ["¼" ")"]
                        ["]" ","]
                        ["\\$" "+"]
                        ["Ö" "×"]
                        ;;["\\&" "×"]
                        ["ö" "÷"]
                        ;;   ["º" "÷"]
                        ["\\^" "‘"]
                        ["\\*" "’"]
                        ["¬" "↑"]]})




(defn move-small-i-matra [text]
  (u/move-key-forward text 0 "ि"))

(defn move-prakash-r [text])

(defn move-parn-r
  "half r position is moved based on prev character. This half-r goes before the consonant पर्ण"
  ;;सवेर्श --> सर्वेश
  [text]
  (u/move-key-backward text 0 "र्"))
(defn debug [x] (prn x) x)
(defn kundli-to-unicode [text]
  (-> text
      (u/replace-key-with-val (:double-letter-keys kundli-key))
      (u/replace-key-with-val (:single-letter-keys kundli-key))
      (u/replace-key-with-val (:cleanup kundli-key))
      (u/replace-key-with-val (:symbols kundli-key))
      (move-small-i-matra)
      (move-parn-r)
      (u/replace-key-with-val (:cleanup kundli-key))))
