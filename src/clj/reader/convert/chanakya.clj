(ns reader.convert.chanakya
  (:require [clojure.string :as s]
            [reader.convert.font-utils :as u]
            [reader.convert.list :refer [devnagri]]))


(def chanakya-key {:move               ["æ" "ð" "ि"]
                   :consonants         ["क" "ख" "ग" "घ" "ङ" "च" "छ" "ज" "झ" "ञ" "ट" "ठ" "ड" "ढ" "ण" "ड़" "ढ़" "त" "थ" "द" "ध" "न" "प" "फ" "ब" "भ" "म" "य" "र" "ल" "व" "श" "ष" "स" "ह"  "क्ष" "त्र" "ज्ञ" "क़" "ख़" "ग़" "ज़" "ड़" "ढ़" "फ़" "य़"]
                   :symbols            [["Ï" "।"]
                                        ["Ñ" "‘"]
                                        ["Ò" "’"]
                                        ["" "—"]
                                        ["" "‣"]
                                        ["" "/"]
                                        ["\r" "\n  "]
                                        ["\t" "\t"]
                                        ["P" "✓"]]
                   :matra              [["åï" "ो"]
                                        ["ó" "ो"]
                                        ["ô" "ौ"]
                                        ["å" "ा"]
                                        ["æ" "ि"]
                                        ["ç" "ी"]
                                        ["è" "ु" ]
                                        ["é" "ू"]
                                        ["ï" "े"]
                                        ["Ó" "े"]
                                        ["ð" "ै"]
                                        ["¡" "ं"]
                                        ["³" "ं"]
                                        [" " "ँ"]
                                        ["ñ" "ॅ"]
                                        ["ê" "ृ"]
                                        ["ë" "ॄ"]
                                        ["Ð" ":"]
                                        ["ö" "्"]
                                        ["Z" "û³"]]
                   :cleanup            [["्ा" ""]
                                        ["्ो" "े"]
                                        ["ंे" "ें"]
                                        ["ाे" "ो"]
                                        ["ाै" "ौ"]
                                        ["अा" "आ"]
                                        ["अो" "ओ"]
                                        ["आै" "औ"]
                                        ["अौ" "औ"]
                                        ["अों" "ओं"]
                                        ["आॅ" "ऑ"]
                                        ["्ाी" "ी"]
                                        ["एे" "ऐ"]
                                        ["U" ""]
                                        ["î" ""]]
                   :double-letter-keys [["Q£" "¶£öË"]
                                        ["È·" "ढ़"]
                                        ["¦û" "ई"]
                                        ["Ç£" "फ"]
                                        ["½£" "ङ"]
                                        ["¶£" "क"]
                                        ["L£" "रु"]
                                        ["©£" "ऊ"]
                                        ["M£" "रू"]
                                        ["ª£" "ऋ"]
                                        ["Æ·" "ड़"]
                                        ["D" "ष्ठ"]
                                        ["C" "ष्ट"]
                                        ["ÖA" "म्न"]
                                        ["ÅK" "ठ्य"]
                                        ["£¶" "क"]]
                   :triple-letter-keys [["¤åï" "ओ"]]
                   :single-letter-keys [
                                        ["¤" "अ"]
                                        ["¯" "ए"]
                                        ["¦" "इ"]
                                        ["¨" "उ"]
                                        ["ù" "ॐ"]
                                        ["Ù" "र"]
                                        ["â" "ह"]
                                        ["Ä" "ट"]
                                        ["Å" "ठ"]
                                        ;; ["î" "ठ"]
                                        ["È" "ढ"]
                                        ["¿" "छ"]
                                        ["Æ" "ड"]
                                        ["m" "द्व"]
                                        ["â" "ह"]
                                        ["À" "ज"]
                                        ["Á" "प"]
                                        ["Þ" "व"]
                                        ["Ô" "ब"]
                                        ["Õ" "भ"]
                                        ["Ë" "त"]
                                        ["á" "स"]
                                        ["o" "श"]
                                        ["à" "ष"]
                                        ["Í" "द"]
                                        ["Î" "ध"]
                                        ["Ì" "थ"]
                                        ["Ö" "म"]
                                        ["â" "ह"]
                                        ["¾" "च"]
                                        ["½" "ङ"]
                                        ["Û" "ल"]
                                        ["º" "ग"]
                                        ["¸" "ख"]
                                        ["Ø" "न"]
                                        ["Ù" "र"]
                                        ["×" "य"]
                                        ["S" "स्"]
                                        ["»" "द"]
                                        ["c" "ष्"]
                                        ;;["C" "ष्"]
                                        ["ý" "्र"]
                                        ["#" "प्त"]
                                        ["" "क्"]
                                        ["" "ख्"]
                                        ["" "ग्"]
                                        ["" "घ्"]
                                        ["" "ज्"]
                                        ["" "ण्"]
                                        ["" "त्"]
                                        ["" "थ्"]
                                        ["" "ध्"]
                                        ["" "न्"]
                                        ["" "प्"]
                                        ["" "फ्"]
                                        ["" "ब्"]
                                        ["" "भ्"]
                                        ["" "म्"]
                                        ["" "च्"]
                                        ["" "ज्"]
                                        ["" "च्च्"]
                                        ["\230" "ज्ञ्"]
                                        ["" "त्र्"]
                                        ["" "त्त्"]
                                        ["" "श्र्"]
                                        ["" "ञ्"]
                                        ["°" "ह्"]
                                        ["ø" "ऽ"] ;;avagraha
                                        ["@" "ञ्च"]
                                        ["p" "श्च"]
                                        ["X" "ङ्ग"]
                                        ["V" "ङ्क"]
                                        ["þ" "क्ष्"]
                                        ["l" "द्य"]
                                        ["h" "द्ध"]
                                        ["N" "हृ" ]
                                        ["O" "ह्र"]
                                        ["d" "स्त्र"]
                                        ["Ê" "ल्"]
                                        ["Â" "व्"]
                                        ["ß" "श्"]
                                        ["Ú" "झ्"]
                                        ["m" "द्व"]
                                        ["k" "द्म"]
                                        ["r" "ह्म्"]
                                        ["" "ल्ल"]
                                        ["" "ज्ज्"]
                                        ["\\^" "ट्ट"]
                                        ["g" "द्द"]
                                        ["a" "ड्ड"]
                                        ["n" "ठ्ठ"]
                                        ["_" "ट्ठ"]
                                        ["ò" "न्न्"]]
                   :numbers            [["­" "0"]
                                        ["v" "1"]
                                        ["w" "2"]
                                        ["x" "3"]
                                        ["y" "4"]
                                        ["z" "5"]
                                        ["\\{" "6"]
                                        ["\\|" "7"]
                                        ["}" "8"]
                                        ["~" "9"]
                                        ]
                   :order              [:triple-letter-keys :double-letter-keys :single-letter-keys :matra :numbers :cleanup :symbols
                                        ]})


(defn char-list []
  (map second (apply concat (vals (dissoc chanakya-key :order :move :consonants :symbols :numbers)))))

(defn replace-values [text keys-to-replace]
  (if (empty? keys-to-replace)
    text
    (let [remaining-keys (rest keys-to-replace)
          [key value]    (first keys-to-replace)]
      (replace-values
       (s/replace text (re-pattern key) value)
       (rest keys-to-replace)))))

(defn get-keys-in-order
  ([cipher-key] (get-keys-in-order cipher-key (:order cipher-key)))
  ([cipher-key order]
   (reduce
    (fn [acc k]
      (concat acc (k cipher-key)))
    []
    order)))

(defn replace-values-in-order [text cipher-key]
  (replace-values text (get-keys-in-order cipher-key)))

(defn transform-double-letter-keys [text double-letter-consonants]
  (let [t (replace-values text double-letter-consonants)]
    (reduce
     (fn [acc [c _]]
       (let [[v1 v2]  (s/split c #"")
             pattern1 (re-pattern (str v1 #"\S" v2))
             pattern2 (re-pattern (str v1 #"\S\S" v2))]
         ;;       (prn "===" c v1 v2  acc)
         (s/replace
          (s/replace
           acc
           pattern1
           #(s/join [v1 v2 (subs %1 1 2)]))
          pattern2
          #(s/join [v1 v2 (subs %1 1 3)]))))
     t
     double-letter-consonants)))

(defn move-one-key [full-string curr-index key-to-move all-keys matras]
  (let [pos-   (s/index-of (subs full-string curr-index) key-to-move)
        length (count full-string)]
    (if (and pos- (not= (+ curr-index pos-) (dec length)))
      (let [pos              (+ pos- curr-index)
            next-char        (first (filter
                                     #(= % (subs
                                            full-string
                                            (inc pos)
                                            (min length (+ 1 pos (count %)))))
                                     all-keys))
            new-index-v1     (min length (+ pos (count key-to-move) (count next-char)))
            after-next-char  (subs full-string new-index-v1 (min length (inc new-index-v1)))
            is-halant        (= after-next-char  "्")
            new-index        (+ new-index-v1 (if is-halant (+ 1 (count after-next-char)) 0))
            second-next-char (first (filter
                                     #(= % (subs
                                            full-string
                                            new-index
                                            (min length (+ new-index (count %)))))
                                     all-keys))

            ;;            _              (prn "is matra" is-halant after-next-char new-index second-next-char)
            updated-string (str
                            (subs full-string 0 pos) next-char
                            (subs full-string new-index-v1 new-index)
                            key-to-move
                            (subs full-string new-index))]
        (move-one-key
         updated-string
         new-index
         key-to-move
         all-keys
         matras))
      full-string)))

(defn move-keys [full-string cipher-key]
  (let [keys-to-move (:move cipher-key)
        all-keys     (concat
                      (:consonants cipher-key)
                      (map second (get-keys-in-order cipher-key (disj (set (:order cipher-key)) :symbols :cleanup :matras))))
        matras       (flatten (:matra cipher-key))]
    (reduce
     (fn [acc v]
       (move-one-key acc 0 v all-keys matras))
     full-string
     keys-to-move)))

(defn add-half-r-1
  "half r position is moved based on prev character. This half-r goes before the consonant"
  [full-string curr-index]
  (let [considered-str (subs full-string curr-index)
        half-r-pos     (s/index-of (subs full-string curr-index) "û")]
    (if half-r-pos
      (let [split-string         (reverse (subs full-string curr-index (+ curr-index half-r-pos)))
            prev-consonant-index (inc (.indexOf (map u/is-matra? split-string) false))
            start-pos            (+ curr-index (max 0 (- half-r-pos prev-consonant-index)))
            end-pos              (+ curr-index half-r-pos)]
        (add-half-r-1
         (str
          (subs full-string 0 curr-index)
          (subs full-string curr-index start-pos)
          "र्"
          (subs full-string start-pos end-pos)
          (subs full-string (inc end-pos)))
         (+ curr-index half-r-pos)))
      full-string)))

(defn add-half-r-2
  "half r position is moved based on prev character प्रभारी"
  [full-string curr-index]
  (let [matras     ["ा" "ि" "ी" "ु" "ू" "ृ" "े" "ै" "ो" "ौ" "ं" ":" "ँ" "ॅ"]
        half-r-pos (s/index-of (subs full-string curr-index) "ü")]
    (if half-r-pos
      (let [p              (+ curr-index half-r-pos)
            dec-p          (if (> p 0) (dec p) 0)
            check-if-matra (not= -1 (.indexOf matras (subs full-string dec-p p)))
            start-pos      (if check-if-matra dec-p p)
            character      (subs full-string dec-p p)]
        (add-half-r-2
         (str
          (subs full-string 0 start-pos)
          "्र"
          (subs full-string start-pos p)
          (subs full-string (inc p)))
         (inc p)))
      full-string)))




(defn debug [x debug?] (when (true? debug?) (prn "?" x))x)

(defn part [s a b] (prn (subs s a b)) (subs s a b))

(defn decipher-string
  ([text cipher-key] (decipher-string text cipher-key false))
  ([text cipher-key debug?]
   (-> text
       (debug debug?)
       (transform-double-letter-keys (:double-letter-keys cipher-key))
       (debug debug?)
       (replace-values-in-order cipher-key)
       (debug debug?)
       (move-keys  cipher-key)
       (debug debug?)
       (add-half-r-1 0)
       (add-half-r-2 0)
       (replace-values-in-order  cipher-key))))

(defn chanakya-to-unicode [text]
  (decipher-string text chanakya-key))

(defn convert
  ([text a b]
   (prn (subs text a b))
   (print (decipher-string (subs text a b) chanakya-key))
   nil)
  ([text]
   (print (decipher-string text chanakya-key))
   nil))

(defn sp [text]
  (s/split text #""))


(defn find [text char]
  (let [pattern (re-pattern (str "\\b(?=\\w*" char ")\\w+\\b"))
        pattern (re-pattern (str "......" char "......"))]
    (prn pattern)
    (re-seq pattern text)))


;;; finding all letters are present in the key
(defn find-missing-letters []
  (let [letters-present   (reduce
                           concat
                           (vals
                            (select-keys
                             chanakya-key
                             [:matra :single-letter-keys :double-letter-keys :triple-letter-keys])))
        reverse-letters   (reduce
                           (fn [acc v]
                             (assoc acc (last v) (first v))) {} letters-present)
        devnagri-unicode  (reduce (fn [acc [k v]] (assoc acc k {:unicode v})) {} devnagri)
        devnagri-chanakya (reduce
                           (fn [acc [k v]]
                             (assoc acc k
                                    (merge v {:chanakya (get reverse-letters k)})))
                           {}
                           devnagri-unicode)
        missing-letters   (sort
                           #(compare (:unicode (last %1)) (:unicode (last %2)))
                           (filter
                            #(nil? (:chanakya (last %)))
                            devnagri-chanakya))]
    (clojure.string/join "  " (map first missing-letters))))
