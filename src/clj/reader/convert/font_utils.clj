(ns reader.convert.font-utils
  (:require [clojure.string :as s]))

(def varnamala
  {:numbers    [ "०" "१" "२" "३" "४" "५" "६" "७" "८" "९"]
   :symbols    [ "।" "॥"]
   :matra      [ "ॢ"  "ॣ"
                "ी"  "ु" "ू" "ृ" "ॄ" "ॅ" "ॆ" "े" "ै" "ॉ" "ॊ" "ो" "ौ" "्" "ॎ" "ॏ"
                "ऺ" "ऻ" "़" "ऽ"  "ा" "ि" "ऀ"  "ँ" "ं" "ः"]
   :consonants ["क" "ख" "ग" "घ" "ङ"
                "च" "छ" "ज" "झ" "ञ"
                "ट" "ठ" "ड" "ढ" "ण" "ड़" "ढ़"
                "त" "थ" "द" "ध" "न" "ऩ"
                "प" "फ" "ब" "भ" "म"
                "य" "र" "ऱ" "ल" "व"
                "श" "ष" "स" "ह"
                "क्ष" "त्र" "ज्ञ"
                "ॠ"]
   :complex    ["रु"  "रू" "क़"  "ख़" "ग़" "ज़" "ड़" "ढ़" "फ़" "य़"  "ॡ"  "ॐ"
                "द्व" "त्र्" "त्त्" "श्र्" "द्य" "द्ध" "हृ" "ह्र" "द्म" "ह्म्" "ट्ट" "द्द"]
   :vowel      ["ऄ" "अ" "आ" "इ" "ई"  "उ" "ऊ" "ऋ" "ऌ" "ऍ" "ऎ" "ए" "ऐ" "ऑ"
                "ऒ"  "ओ" "औ"]})

(def varnamala-list (flatten (vals varnamala)))

(defn is-matra? [x] (not= -1 (.indexOf (:matra varnamala) (str x))))

(defn replace-key-with-val [text remaining-key-maps]
  (if (empty? remaining-key-maps)
    text
    (let [[k v] (first remaining-key-maps)]
      (replace-key-with-val
       (s/replace text (re-pattern k) v)
       (rest remaining-key-maps)))))

(defn move-key-backward [text curr-index key-to-move]
  (let [text-to-check (subs text curr-index)
        pos           (s/index-of text-to-check key-to-move)
        length        (count text-to-check)
        in-bound?     (fn [index] (< index length))
        bound         (fn [index] (cond (< index 0) 0 (>= index length) length :else index))]
    (if (and pos (in-bound? pos))
      (if-let [prev-char (first (filter
                                 #(= % (subs
                                        text-to-check
                                        (max 0 (- pos (count %)))
                                        pos))
                                 varnamala-list))]
        (let [moving-chunk-start      (- pos (count prev-char))
              moving-chunk-end        pos
              is-prev-char-matra?     (is-matra? prev-char)
              halant-before-prev-char (= "्" (subs text-to-check (dec moving-chunk-start) moving-chunk-start))
              new-index               (if halant-before-prev-char
                                        curr-index
                                        (if is-prev-char-matra?
                                          curr-index
                                          (+ curr-index pos (count key-to-move))))
              updated-text            (str (subs text 0 curr-index)
                                           (subs text-to-check 0 moving-chunk-start)
                                           key-to-move
                                           (subs text-to-check moving-chunk-start pos)
                                           (subs text-to-check (+ pos (count key-to-move))))]
          (move-key-backward
           updated-text
           new-index
           key-to-move))
        text)
      text)))

(defn move-key-forward [text curr-index key-to-move]
  (let [text-to-check (subs text curr-index)
        pos           (s/index-of text-to-check key-to-move)
        length        (count text-to-check)
        in-bound?     (fn [index] (< index length))
        bound         (fn [index] (cond (< index 0) 0 (>= index length) length :else index))]
    (if (and pos (in-bound? pos))
      (if-let [next-char (first (filter
                                 #(= % (subs
                                        text-to-check
                                        (inc pos)
                                        (min length (+ 1 pos (count %)))))
                                 varnamala-list))]
        (let [moving-chunk-start (+ pos (count key-to-move))
              moving-chunk-end   (+ pos (count key-to-move) (count next-char))
              halant-after-char  (= "्" (subs text-to-check moving-chunk-end (bound (inc moving-chunk-end))))
              moving-chunk-end   (if halant-after-char (inc moving-chunk-end) moving-chunk-end)
              new-index          (if halant-after-char
                                   (+ curr-index (bound (- (dec moving-chunk-end) (count key-to-move))))
                                   (+ curr-index (bound (+ (dec moving-chunk-end) (count key-to-move)))))
              updated-text       (str (subs text 0 curr-index)
                                      (subs text-to-check 0 pos)
                                      (subs text-to-check moving-chunk-start moving-chunk-end)
                                      key-to-move
                                      (subs text-to-check moving-chunk-end))]
          (move-key-forward
           updated-text
           new-index
           key-to-move))
        text)
      text)))
