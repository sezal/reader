(ns reader.convert.pm6
  (:require [reader.convert.pagemaker-buffer :as pm6]
            [reader.convert.construct :as construct]
            [reader.convert.records :as rec]
            [reader.convert.parse :as parse]
            [reader.convert.question :as question]
            [reader.domain.file :as file-domain]
            [reader.rabbitmq.publisher :refer [publish-message]]
            [clojure.java.io :as io]
            [clojure.stacktrace :as st]
            [taoensso.timbre :as timbre]
            [schema.core :as s]
            [reader.validate :as v]))

(s/defschema ConvertP65FileRequest
  {:file-location s/Str
   :file-id       s/Uuid})

(defn list-records [file]
  (let [{:keys [toc-offset toc-length]} (:header file)]
    (assoc file :records (->> (rec/read-next-record-toc file toc-offset toc-length 0 false 0)
                              second
                              flatten
                              parse/add-type-detail))))

(defn parse-records [file]
  (let [records (:records file)]
    (map #(assoc % :parsed-data (parse/parse-records file %)) records)))

(defn convert-file->records [file]
  (let [file-data {:buffer (pm6/open-gsf file)}]
    (-> file-data
        rec/parse-header
        list-records
        parse-records)))

#_(defn parse-questions-in-document [document]
    (timbre/info "type of document" (type document) document)
    (question/parse-questions (vec document)))

(defn convert-records->document [records]
  (map :formatted-text (construct/construct-document records)))

(defn get-name [name extension]
  (str name "." extension))

(defn get-temp-file [extension]
  (.getAbsolutePath (java.io.File/createTempFile "temp" extension)))

(defn convert-job [data]
  (timbre/info "converting to unicode" data)
  (let [{:keys [file-id file-location]} (v/validate ConvertP65FileRequest data)
        records                         (convert-file->records file-location)
        document                        (clojure.string/join (convert-records->document records))
        ;; document-with-questions        (question/parse-questions document)
        json-file-path                  (get-temp-file ".json")
        txt-file-path                   (get-temp-file ".txt")]
    (spit json-file-path records)
    (publish-message {:source-file-location json-file-path
                      :file-uuid            file-id
                      :file-type            :records} "upload")
    (spit txt-file-path document)
    (publish-message {:source-file-location txt-file-path
                      :file-uuid            file-id
                      :file-type            :text} "upload")
    (timbre/info "conversion to unicode complete" data)))

#_(def questions (map question/get-questions (clojure.string/join formatted-text)))
#_(with-open [w (clojure.java.io/writecr  new-file :append true)]
    (.write w (str (clojure.string/join formatted-text))))


(def filename "/Users/sezaljain/Documents/pagemaker/Rajasthan_selected_MPJ.p65")

#_(def text (clojure.string/join (map :formatted-text (construct/construct-document (convert-file->records filename)))))

#_(convert-job {:file-id "a2bfe025-8bd2-44d8-a64d-ad8b8dbacd40" :file-location filename})
