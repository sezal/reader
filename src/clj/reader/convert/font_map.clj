(ns reader.convert.font-map
  (:require [clojure.string :as s]
            [reader.convert.kundli :as k]))

(def kundli
  {
   "O;k[;k&"
   "व्याख्या—"
   "'ks\"k rhuksa xaxk dh lgk;d ufn;k  "
   "शेष तीनों गंगा की सहायक नदियाँ"
   "eD[kh ,d dhV gS tcfd rhuksa i{khA"
   "मक्खी एक कीट है जबकि तीनों पक्षी।"
   "fn fdlh lkadsfrd Hkk\"kk esa dks fy[kk tkrk gS] rks mlh Hkk\"kk esa dSls fy[kk tk;sxk"
   "यदि किसी सांकेतिक भाषा में को लिखा  जाता है, तो उसी भाषा में कैसे लिखा जाएगा"
   ";fn fdlh dwV esa nhokj dks f[kM+dh] f[kM+dh dks njoktk] njokts dks Q'kZ] Q'kZ dks Nr vkSj Nr dks jks'kunku dgk tkrk gS] rks O;fDr dgka [kM+k gksxk"
   "यदि किसी कूट में दीवार को खिड़की, खिड़की को दरवाजा, दरवाजे  को फ़र्श, फ़र्श को छत और छत को रोशनदान कहा जाता है, तो व्यक्ति कहां खड़ा होगा"
   "nksuksa izk:iksa esa fcUnq dkWeu gSA vr% ?kM+h dh fn'kk esa ?kwekus ij  ds foijhr  gksxkA"
   "दोनों प्रारूपों में बिंदु कॉमन है। अतः घड़ी की दिशा में घूमाने पर के विपरीत होगा।"
   ",d ekud ikls ij ds ikl okys »fudVorhZ¼ Qyd ij fuEu esa ls dkSulk vad ugha gksxk"
   "एक मानक पासे पर के पास वाले (निकटवर्ती) फलक पर निम्न में से कौनसा अंक नहीं होगा"
   "fo|kfFkZ;ksa dh d{kk esa _rq vkSj रुचि dk ×ekad oka ,oa  gS] rks budk uhps ls ×ekad D;k gksxk"
   "विद्यार्थियों की कक्षा में ऋतु और रुचि का क्रमांक वां एवं है, तो इनका नीचे से क्रमांक क्या होगा"
   "ftl izdkj jk\"V©ifr Hkkjr »ns'k¼ dk laoS/kkfud eqf[k;k gksrk gS] mlh izdkj jkT;iky jkT; dk laoS/kkfud eqf[k;k gksrk gS"
   "जिस प्रकार राष्ट्रपति भारत (देश) का संवैधानिक मुखिया होता है, उसी प्रकार राज्यपाल राज्य का संवैधानिक मुखिया होता है"
   "  iSlk lEifØk  ×wj xqLlk d#.kk n;kyqrk cqf)eku  f'k{kk vfHkeku  fouezrk"
   "पैसा सम्पत्ति  क्रूर ग़ुस्सा करुणा दयालुता बुद्धिमान शिक्षा अभिमान विनम्रता "
   "ftl izdkj iSlk lEifØk esa vUrfuZfgr gS] mlh izdkj"
   "जिस प्रकार पैसा सम्पत्ति में अंतर्निहित है, उसी प्रकार"
   "yM+fd;ksa dh ,d iafDr esa deyk vkxs ls oha gSA yhyk lquhrk ls LFkku vkxs gS tks vkxs ls bl iafDr esa deyk vkSj yhyk ds chp fdruh yM+fd;k  gSa"
   "लड़कियों की एक पंक्ति में कमला आगे से वीं है। लीला सुनीता से स्थान आगे है। जो आगे से इस पंक्ति में कमला और लीला के बीच कितनी लड़कियाँ  हैं"
   ",d f×dsV eSp esa dsoy us losZ'k ls vf/kd ijUrq fgrs'k ls de ju cuk,A fgrs'k ds ju nso ds cjkcj ugha Fks] nso us uhjt ls vf/kd ju cuk,] fdlus lcls vf/kd ju cuk;s"
   "एक क्रिकेट मैच में केवल ने सर्वेश से अधिक परन्तु हितेश से कम रन बनाए। हितेश के रन देव के बराबर नहीं थे, देव ने नीरज से अधिक रन बनाए, किसने सबसे अधिक रन बनाए "})
;;"अंतर्निहित"

(defn isolate-mapping [[ font-string unicode-string b]]
  (let [f     (remove s/blank? (s/split font-string #" "))
        u     (remove s/blank? (s/split unicode-string #" "))
        match (zipmap u f)]
    (reduce
     (fn [acc [k v]]
       (let [k- (s/split k #"")
             v- (s/split v #"")]
         (if (= (count k-) (count v-))
           (merge-with into acc {:single (zipmap k- v-)})
           (merge-with into acc {:words {k v}}))))
     {}
     match)))

(defn debug [x] (prn "==" x) x)

#_(defn aa []
    (->> kundli
         ;; (map create-map)
         (map :single)
         ;;       (debug)
         (reduce (fn [acc map-]
                   (let [mm (reduce (fn [acc- [k v]]
                                      (assoc acc- v (conj (get acc- v) k)))
                                    {}
                                    map-)]
                     (merge-with into acc mm))) {})
         (reduce (fn [acc [k v]]
                   (assoc acc k (vec (set v)))) {})))

#_(defn mmm []
    (let [pp (aa)]
      (prn pp)
      (sort-by #(first (map int (char-array (second %)))) > (map #(conj [(first %)] (first (second %))) (remove #(> (count (second %)) 1) pp)))))


(prn (->> kundli
          (map first)
          (map k/kundli-to-unicode)))
