(ns reader.db
  (:require [mount.core :refer [defstate]]
            [reader.config :refer [config]]
            [taoensso.timbre :as timbre]))

(defstate datasource
  :start (:db config)
  :stop (timbre/info "Stopping db"))
