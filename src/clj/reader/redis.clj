(ns reader.redis
  (:require [mount.core :refer [defstate start stop]]))

;;pretending to be redis right now

(defstate token-store
  :start (atom {})
  :stop (reset! token-store {}))

(defn add-token [token user-id]
  (swap! token-store assoc token user-id)
  user-id)

(defn get-user-from-token [token]
  (get @token-store token nil))
