(ns reader.translate.google
  (:import [com.google.cloud.translate.v3 BatchTranslateTextRequest
            TranslationServiceClient BatchTranslateTextRequest LocationName
            GcsSource GcsDestination
            InputConfig OutputConfig]
           java.util.concurrent.TimeUnit)
  (:require [taoensso.timbre :as timbre]
            [mount.core :as mount :refer [defstate]]
            [clojure.string :as s]
            [reader.config :refer [config]]
            [reader.rabbitmq.publisher :refer [publish-message]]))


(defstate translate-client
  :start (do
           (timbre/info "starting translate client")
           (. TranslationServiceClient create))
  :stop (do
          (timbre/info "stopping translate client")
          (.close translate-client)))

(defn translate-request [src-uri dest-uri]
  (let [project-id    (:project-id (:google config))
        location      (:location (:google config))
        parent        (str (. LocationName of project-id location))
        gcs-src       (-> (. GcsSource newBuilder)
                          (.setInputUri src-uri)
                          (.build))
        gcs-dest      (-> (. GcsDestination newBuilder)
                          (.setOutputUriPrefix dest-uri)
                          (.build))
        input-config  (-> (. InputConfig newBuilder)
                          (.setGcsSource gcs-src)
                          (.build))
        output-config (-> (. OutputConfig newBuilder)
                          (.setGcsDestination gcs-dest)
                          (.build))]
    (-> (. BatchTranslateTextRequest newBuilder)
        (.setParent parent)
        (.setSourceLanguageCode "hi")
        (.addTargetLanguageCodes "en")
        (.addInputConfigs input-config)
        (.setOutputConfig output-config)
        (.build))))


(defn translate-file-at-uri [{:keys [file-uuid]}]
  (let [bucket-uri (:bucket-uri (:google config))
        src-uri    (str bucket-uri file-uuid "/document.txt")
        dest-uri   (str bucket-uri file-uuid "/translated/")
        request    (translate-request src-uri dest-uri)
        future-    (.batchTranslateTextAsync translate-client request)]
    (publish-message {:file-id      file-uuid
                      :file-type    "index"
                      :location-url (str dest-uri "index.csv")}
                     "add-location")))

(defn google-translate-job [data]
  (timbre/info "Starting google translate" data)
  (translate-file-at-uri data)
  (timbre/info "Queued google translate"))
