(ns reader.events
  (:require
   [re-frame.core :as re-frame]
   [reader.db :as db]
   [reader.user.events :as user-events]
   [reader.organization.events :as org-events]
   [reader.directory.events :as dir-events]
   [reader.upload.events :as upload-events]
   [reader.pm6-file.events :as pm6-events]
   [reader.editor.events :as editor-events]
   [reader.user.api-data :as user-api-data]
   [reader.organization.api-data :as org-api-data]))

(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

(re-frame/reg-event-fx
 ::navigated-to-home
 (fn [cofx _]
   {:db         (assoc (:db cofx) :active-panel :home-panel)
    :http-xhrio [(user-api-data/user-info)
                 (org-api-data/get-org)]}))


;;on navigation to home, raise request for user, org data
