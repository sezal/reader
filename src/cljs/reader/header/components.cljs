(ns reader.header.components
  (:require [re-frame.core :as re-frame]
            [reader.subs :as subs]
            [reader.upload.components :as upload-views]))

(defn radio [label name value]
  [:div.radio
   [:label
    [:input {:type :radio :name name :value value}]
    label]])

(def type-to-name {"text"         "Original text"
                   "english-text" "Translated text"})

(defn type-selector []
  (let [file       (re-frame/subscribe [:reader.pm6-file.subs/selected-file])
        file-types (re-frame/subscribe [:reader.pm6-file.subs/selected-file-available-types (:id @file)])
        types      (->> @file-types
                        (map :type)
                        (map #(get type-to-name %))
                        (remove nil?)
                        sort)]
    (when @file
      [:div {:style {:flex-direction "row"
                     :display        "flex"}}
       (map #(radio % :file-type %) types)])))

(defn directory-header []
  (let [dir        (re-frame/subscribe [:reader.directory.subs/selected-directory])
        file       (re-frame/subscribe [:reader.pm6-file.subs/selected-file])
        file-types (re-frame/subscribe [:reader.pm6-file.subs/selected-file-available-types (:id @file)])
        org        (re-frame/subscribe [:reader.organization.subs/organization])]
    [:div.row
     [:div.column
      {:style {:max-width "70%"
               :width     "auto"}}
      (when @dir
        [:div.heading
         [:dir-heading
          (str
           (clojure.string/join "/" (:path @dir))
           (when @file (str "/"(:name @file))))]
         ;;[:div @file-types]
         ;;[type-selector]
         ])]
     [:div.column
      {:style {:max-width "30%"}}
      [:div.toolbar
       #_[:div.toolbar-item
          (if (:id @file)
            {:on-click #(re-frame/dispatch [:reader.pm6-file.events/file-translation-request (:id @file) (:id @org) (:id @dir)])}
            {:style {:border "2px solid #e7e7ef"
                     :color  "#e7e7ef"}})
          "Translate to English"]
       [:div.toolbar-item
        (if (:id @file)
          {:on-click #(re-frame/dispatch [:reader.editor.events/download-file-data])}
          {:style {:border "2px solid #e7e7ef"
                   :color  "#e7e7ef"}})
        "Download"]]]]))
