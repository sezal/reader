(ns reader.editor.components
  (:require [re-frame.core :as re-frame]
            [reader.editor.subs :as subs]
            ["react-quill" :as Quill]
            ["react-quill" :default ReactQuill]
            [clojure.string :as str]))

#_(defn quill-editor []
    (let [text-form (re-frame/subscribe [::subs/text-form-data])]
      [:div {:style
             {:height     "100%"
              :overflow-y "scroll"}}
       [:> Quill
        {:id          "qu"
         :modules     {:toolbar [[{ :header [1, 2, false] }]
                                 ["bold" "italic" "underline"]]}
         :value       @text-form
         :placeholder "Click on a file to see the extracted text from pagemaker. If you are not able to see the file you hv uploaded, plz email at sezal@nilenso.com"
         :theme       "snow"
         :onChange    #(do
                         (re-frame/dispatch [:reader.editor.events/text-form-changed 124 (. %4 getContents)])
                         (re-frame/dispatch [:reader.editor.events/text-changed 124 (. %4 getText)]))
         }]]))

(defn md-editor []
  (let [text-form (re-frame/subscribe [::subs/text-form-data])]
    (fn []
      [:textarea.editor
       {:id          "input-hindi"
        :value       @text-form
        :readOnly    "readonly"
        :placeholder "Click on a file to see the extracted text from pagemaker. If you are not able to see the file you hv uploaded, plz email at sezal@nilenso.com"}])))

(defn eng-panel []
  (let [eng-text (re-frame/subscribe [::subs/english-translation-data])]
    (fn []
      [:textarea.editor
       {:id          "input-eng"
        :value       @eng-text
        :readOnly    "readonly"
        :placeholder "File has not been translated to english yet"}])))

(defn preview []
  (let [preview-form (re-frame/subscribe [::subs/preview-form-data])]
    [:div
     {:style {:border-width "1px"
              :border-style "solid"
              :height       "100vh"
              :border-color "#9b4dca"
              :padding      "10px"
              :overflow     "scroll"}}
     @preview-form]))

(defn download-button []
  (let [a 1]
    [:button {:type     "submit"
              :style    {:margin "50px"
                         :color  "#000060"
                         :float  "right"
                         }
              :on-click (fn [e]
                          (.preventDefault e)
                          (re-frame/dispatch [:reader.editor.events/download-file-data]))}
     "Download Hindi, English files"]))

(defn download-json-button []
  (let [a 1]
    [:button {:type     "submit"
              :on-click (fn [e]
                          (.preventDefault e)
                          (re-frame/dispatch [:reader.editor.events/create-questions-json]))}
     "download parsed questions as json"]))

(defn editor []
  [:div.panel
   [md-editor]
   #_[quill-editor]
   #_[eng-panel]
   #_[:div.column [preview]]])
