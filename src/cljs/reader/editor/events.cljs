(ns reader.editor.events
  (:require [re-frame.core :as re-frame]
            [hickory.core :refer [parse as-hiccup]]
            [markdown.core :as m]
            [reader.editor.question :as q]))

(defn md->hiccup
  "Accepts a markdown string and returns a hiccup data structure converted from that markdown.
  Also accepts an optional params map. Use the :encode? boolean key to specify whether
  or not you want html escape characters to be encoded. Example:
    (md->hiccup \"#Title\" {:encode? true})"
  [md-str]
  (let [html   (m/md->html md-str :custom-transformers [q/convert-question q/convert-option])
        dom    (parse html)
        hiccup (first (as-hiccup dom))]
    hiccup))

(re-frame/reg-event-db
 ::text-form-changed
 (fn [db [_ file-id value]]
   (-> db
       (assoc-in [:form-data file-id :text-form] value))
   #_(let [;;hiccup-data (md->hiccup value)
           ]
       (-> db
           (assoc-in [:form-data file-id :text-form] value)
           ;; (assoc-in [:form-data file-id :preview-form] hiccup-data)
           ))))

(re-frame/reg-event-db
 ::english-text-changed
 (fn [db [_ file-id value]]
   (assoc-in db [:form-data file-id :text-english] value)))

(re-frame/reg-cofx
 :questions-hiccup
 (fn [coeffects _]
   (assoc
    coeffects
    :questions-hiccup
    (get-in (:db coeffects) [:preview-form :data]))))

(re-frame/reg-cofx
 :questions-filename
 (fn [coeffects _]
   (let [filename  (get-in (:db coeffects) [:selected-file :name])
         json-file (if filename
                     (clojure.string/replace filename #"p65" "json")
                     "myfile.json")]
     (assoc coeffects :questions-filename json-file))))

(re-frame/reg-event-fx
 ::create-questions-json
 [(re-frame/inject-cofx :questions-hiccup) (re-frame/inject-cofx :questions-filename)]
 (fn [cofx _]
   (let [hiccup-data (:questions-hiccup cofx)
         json-data   (q/filter-recursive hiccup-data)
         filename    (:questions-filename cofx)
         db          (:db cofx)]
     (q/download-object-as-json (clj->js json-data) filename)
     {:db
      (-> db
          (assoc-in [:preview-form :questions-json] json-data))})))

(defn download-object [value export-name]
  (let [data-blob (js/Blob. #js [value] #js {:type "application/json"})
        link      (.createElement js/document "a")]
    (set! (.-href link) (.createObjectURL js/URL data-blob))
    (.setAttribute link "download" export-name)
    (.appendChild (.-body js/document) link)
    (.click link)
    (.removeChild (.-body js/document) link)))

(re-frame/reg-event-db
 ::download-file-data
 (fn [db _]
   (let [file-id  (get db :active-form)
         name     (get-in db [:selected :org :directory :file :name])
         filename (str name ".txt")
         text     (get-in db [:form-data file-id :text-form])
         ;;         english-text (get-in db [:form-data file-id :text-english])
         ]
     (download-object text filename)
     ;;   (download-object english-text eng-filename)
     db)))
