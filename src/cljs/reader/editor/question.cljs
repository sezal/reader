(ns reader.editor.question
  (:require [clojure.string :as st]))

(def question-identifier #"!Q")
(def option-identifier #"!A[0-9]")
(def correct-option-identifer "!A0")
(def hint-identifier "!H")
(def source-identifier "!S")
(def detail-answer-identifier "!D")

(def question-tag ["<div class=question><ol><question-text>" "</ol></div>"])
(def option-tag ["<li><question-option>" "</question-option></li>"])
(def correct-option-tag ["<li><question-correct-option>" "</question-correct-option></li>"])
(def question-text-tag ["<question-text>" "</question-text>"])
(def question-hint-tag ["<question-hint>" "</question-hint>"])
(def question-source-tag ["<question-source>" "</question-source>"])
(def question-answer-tag ["<question-answer>" "</question-answer>"])

(defn end-option [text {:keys [prev-option-correct option question eof ending-option-coz-question last-line-empty?] :as state}]
  ;;prev correct option --> correct tag close
  (let [end-option-tag (if prev-option-correct (last correct-option-tag) (last option-tag))
        identifier     (if ending-option-coz-question question-identifier option-identifier)
        has-identifier (re-find (re-pattern identifier) text)
        empty-line     (or last-line-empty? eof (st/blank? text))]
    (cond
      (and option has-identifier)
      [(st/replace-first text (re-pattern identifier) (str end-option-tag has-identifier))
       (dissoc state :option :prev-option-correct :ending-option-coz-question)]
      (and option empty-line)
      [(str text end-option-tag) (dissoc state :option :prev-option-correct :ending-option-coz-question)]
      :else
      [text (dissoc state :ending-option-coz-question)])))

(defn convert-option [t {:keys [eof last-line-empty? next-line option question prev-option-correct] :as state}]
  (let [text            (st/trim t)
        opt-count       (count (re-seq (re-pattern option-identifier) text))
        is-first-option (and question (not option))
        start-option    (if is-first-option
                          (str (last question-text-tag) (first option-tag))
                          (first option-tag))
        this-line-empty (st/blank? text)
        prev-tag        (if prev-option-correct correct-option-tag option-tag)]
    (cond
      (and
       option
       (or last-line-empty? eof this-line-empty))
      (if this-line-empty
        (end-option text state)
        (apply convert-option (end-option text state)))
      (= 0 opt-count) (do  [text state])
      :else           (let [[text- state-]    (end-option text state)
                            is-correct-option (= correct-option-identifer (re-find (re-pattern option-identifier) text-))
                            tag               (if is-correct-option correct-option-tag option-tag)
                            opt-tag           (if is-first-option
                                                (str (last question-text-tag) (first tag))
                                                (first tag))
                            new-text          (st/replace-first text- (re-pattern option-identifier) opt-tag)
                            new-state         (-> state-
                                                  (assoc :option true)
                                                  (assoc :prev-option-correct is-correct-option))]
                        (if (= 1 opt-count)
                          [new-text new-state]
                          (convert-option new-text new-state))))))

(defn end-question [text {:keys [prev-option-correct option question eof] :as state}]
  ;;prev correct option --> correct tag close
  (let [end-question-tag (last question-tag)
        has-question     (re-find (re-pattern question-identifier) text)]
    (cond
      (and question (or eof has-question))
      (let [[t s] (end-option text (assoc state :ending-option-coz-question true))
            state (dissoc s :question)
            text  (if eof
                    (str t end-question-tag)
                    (st/replace-first t (re-pattern question-identifier) (str end-question-tag has-question)))]
        [text state])
      :else [text state])))

(defn convert-question [t {:keys [eof last-line-empty? question option] :as state}]
  (let [text            (st/trim t)
        ques-count      (count (re-seq (re-pattern question-identifier) text))
        this-line-empty (st/blank? text)]
    (cond
      (and question (or last-line-empty? eof this-line-empty))
      (let [new-text  (if option
                        (str (last option-tag) (last question-tag) text)
                        (str (last question-tag) text))
            new-state (dissoc state :question :option)]
        (if this-line-empty
          (end-question text state)
          (apply convert-question (end-question text state))))
      (= 0 ques-count) [text state]
      :else            (let [[text- state-] (end-question text state)
                             ques-tag       (first question-tag)
                             new-text       (st/replace-first text- (re-pattern question-identifier) ques-tag)
                             new-state      (-> state-
                                                (assoc :question true))]
                         (if (= 1 ques-count)
                           [new-text new-state]
                           (convert-question new-text new-state))))))


(defn transform-question [data]
  (let [question (atom {:options []})]
    (clojure.walk/prewalk (fn [el]
                            (when (and (vector? el)
                                       (keyword? (first el))
                                       (re-find #"question" (name (first el))))
                              (let [is-option (re-find #"option" (name (first el)))
                                    value     (first (filter string? el))]
                                (if is-option
                                  (swap! question update-in [:options] conj {(first el) value})
                                  (swap! question assoc (first el) value))
                                ))
                            el)
                          (drop 2 data))
    @question))

(defn filter-recursive [coll]
  (let [matches (atom [])]
    (clojure.walk/postwalk (fn [el]
                             (when (and (vector? el) (= :div (first el)) (= (second el) {:class "question"}))
                               (let [data- (transform-question el)]
                                 (swap! matches conj data-)))
                             el)
                           coll)
    @matches))


(defn to-json [v] (.stringify js/JSON v))

(defn download-object-as-json [value export-name]
  (let [data-blob (js/Blob. #js [(to-json value)] #js {:type "application/json"})
        link      (.createElement js/document "a")]
    (set! (.-href link) (.createObjectURL js/URL data-blob))
    (.setAttribute link "download" export-name)
    (.appendChild (.-body js/document) link)
    (.click link)
    (.removeChild (.-body js/document) link)))


#_(download-object-as-json (clj->js {:hello "world"}) "myfile.json")
