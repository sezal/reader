(ns reader.editor.subs
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::text-form-data
 (fn [db _]
   (let [file-id (:active-form db)]
     (get-in db [:form-data file-id :text-form]))))

(re-frame/reg-sub
 ::preview-form-data
 (fn [db _]
   (get-in db [:form-data :preview-form])))

(re-frame/reg-sub
 ::active-form
 (fn [db _]
   (:active-form db)))

(re-frame/reg-sub
 ::english-translation-data
 (fn [db _]
   (get-in db [:form-data  :text-english])))
