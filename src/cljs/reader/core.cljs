(ns reader.core
  (:require
   [reagent.core :as reagent]
   [re-frame.core :as re-frame]
   [reader.events :as events]
   [reader.views :as views]
   [reader.config :as config]))

(defn dev-setup []
  (when config/debug?
    (enable-console-print!)))

(defn ^:dev/after-load mount-root []
  (re-frame/clear-subscription-cache!)
  (re-frame/dispatch [::events/navigated-to-home])
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn init []
  (re-frame/dispatch-sync [::events/initialize-db])
  (dev-setup)
  (mount-root))
