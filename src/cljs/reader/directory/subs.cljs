(ns reader.directory.subs
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::org-id
 (fn [db _]
   ;;assuming there is only one org for a user
   (get-in db [:selected :org :id])))

(re-frame/reg-sub
 ::org-directories
 (fn [db [_ org-id]]
   (get-in db [:data :directories])))

(re-frame/reg-sub
 ::is-selected-directory
 (fn [db [_ dir-id]]
   (and dir-id (= dir-id (get-in db [:selected :org :directory :id])))))

(re-frame/reg-sub
 ::selected-directory
 (fn [db _]
   (get-in db [:selected :org :directory])))

(re-frame/reg-sub
 ::selected-directory-path
 (fn [db _]
   (get-in db [:selected :org :directory :path])))

(re-frame/reg-sub
 ::add-new-directory
 (fn [db [_]]
   (get-in db [:add-new-directory-form])))

(re-frame/reg-sub
 ::add-new-directory-failure
 (fn [db [_]]
   (get-in db [:add-new-directory-form :submit-status])))

#_(re-frame/reg-sub
   ::add-sub-directory
   (fn [db [_ dir-id]]
     (and dir-id (= dir-id (get-in db [:add-sub-directory-form :parent-dir])))))

#_(re-frame/reg-sub
   ::add-sub-directory-failure
   (fn [db _]
     (get-in db [:add-sub-directory-form :submit-status])))

#_(re-frame/reg-sub
   ::add-sub-directory-form
   (fn [db _]
     (:add-sub-directory-form db)))
