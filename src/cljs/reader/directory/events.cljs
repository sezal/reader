(ns reader.directory.events
  (:require [re-frame.core :as re-frame]
            [reader.directory.api-data :as api-data]
            [reader.pm6-file.api-data :as pm6-file-api]))

(enable-console-print!)

(re-frame/reg-event-db
 ::received-directory-list
 (fn [db [_ directories]]
   (let [org-id (:org-id (first directories))]
     (assoc-in db [:data :directories] directories))))

(re-frame/reg-event-fx
 ::get-directories
 (fn [cofx [_ org-id]]
   {:http-xhrio (api-data/get-directories org-id)}))

(re-frame/reg-event-fx
 ::directory-selected
 (fn [{db :db} [_ dir-id path org-id]]
   (if dir-id
     {:db         (-> db
                      (assoc-in [:selected :org :directory ] {:id dir-id :path path})
                      (assoc :active-form nil))
      :http-xhrio (pm6-file-api/get-files org-id dir-id)}
     {:db (assoc-in db [:selected :org :directory] {})})))

(re-frame/reg-event-db
 ::add-new-dir
 (fn [db [_]]
   (update db :add-new-directory-form not)))

(re-frame/reg-event-fx
 ::create-directory-submit
 (fn [{db :db} [_ dir-data]]
   {:db         db
    :http-xhrio (api-data/create-directory dir-data)}))

(re-frame/reg-event-fx
 ::create-directory-success
 (fn [{db :db} [_ dir-data]]
   {:db         (assoc db :add-new-directory-form false)
    :http-xhrio (api-data/get-directories (:org-id dir-data))}))

(re-frame/reg-event-db
 ::create-directory-failure
 (fn [db [_ dir-data]]
   (let [err-msg (if (get-in dir-data [:response :spec]) "invalid input"
                     (get-in dir-data [:parse-error :original-text]))]
     (-> db
         (assoc-in [:add-new-directory-form :submit-status :error] true)
         (assoc-in [:add-bew-directory-form :submit-status :message] err-msg)))))
