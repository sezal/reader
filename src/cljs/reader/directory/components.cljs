(ns reader.directory.components
  (:require [reagent.core :as r]
            [re-frame.core :as re-frame]
            [reader.directory.subs :as subs]
            [reader.pm6-file.components :as file-views]))

(defn add-top-level-directory []
  (let [org               (re-frame/subscribe [::subs/org-id])
        show-add-dir-form (re-frame/subscribe [::subs/add-new-directory])
        add-dir-error     (re-frame/subscribe [::subs/add-new-directory-failure])
        dir-details       (atom {:org-id @org})]
    [:div
     [:div.btn {:on-click #(re-frame/dispatch [:reader.directory.events/add-new-dir])}
      "Add directory" [:i.icon [:i.fa.fa-plus]]]
     (when @show-add-dir-form
       [:div.row
        [:div.column
         {:class "column column-80"}
         [:input {:type      "text"
                  :style     {:color         "#FFFFFF"
                              :margin-bottom 0}
                  :class     [(if (:error @add-dir-error) :error-form)]
                  :on-change (fn [e] (swap! dir-details assoc :name (-> e .-target .-value)))}]]

        [:div.column
         {:class "column column-10"
          :style {:padding 0}}
         [:button
          {:style    {:padding-left  "5px"
                      :padding-right "5px"
                      :margin-bottom 0}
           :on-click #(re-frame/dispatch [:reader.directory.events/create-directory-submit @dir-details])}
          "→"]]])]))

(defn directory [{:keys [id name org-id path] :as dir}]
  (let [selected (re-frame/subscribe [::subs/is-selected-directory id])]
    (fn []
      (if @selected
        [:div.directory-selected
         [:span
          {:id       id
           :on-click #(re-frame/dispatch [:reader.directory.events/directory-selected id path org-id])}
          name]
         (file-views/files-for-directory org-id id)]
        [:div.directory
         [:span {:id       id
                 :on-click #(re-frame/dispatch [:reader.directory.events/directory-selected id path org-id])}
          name]]))))

(defn directory-collection->directory-element [{:keys [id directories name] :as dir-coll} path]
  "create a hiccup element out of directory collection. A map within the collection is mapped to 'li' and a list is mapped to 'ul'"
  (if (nil? dir-coll)
    nil
    (if (map? dir-coll)
      (let [path (conj path name)]
        ^{:key id} [:li [directory (assoc dir-coll :path path)] (directory-collection->directory-element directories path)])
      [:ul.dir-list (map #(directory-collection->directory-element % path) dir-coll)])))

(defn directory-list [org-id]
  (let [directories (re-frame/subscribe [::subs/org-directories org-id])]
    (directory-collection->directory-element @directories [])))
