(ns reader.user.subs
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::user
 (fn [db _]
   (get-in db [:user :data])))
