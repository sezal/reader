(ns reader.user.api-data
  (:require [ajax.core :as ajax]
            [re-frame.core :as re-frame]))

(enable-console-print!)

(defn user-info []
  {:method          :get
   :uri             "/user-info"
   :response-format (ajax/json-response-format {:keywords? true})
   :on-success      [:reader.user.events/user-info-retrieved]})
