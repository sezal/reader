(ns reader.user.components
  (:require [re-frame.core :as re-frame]
            [reader.user.subs :as subs]))

(enable-console-print!)

(defn greeting []
  (let [user-info (re-frame/subscribe [::subs/user])]
    (fn []
      (let [name (:first-name @user-info)]
        [:div [:name name]
         [:orgname "Lakshya"]]))))
