(ns reader.panel.components
  (:require [re-frame.core :as re-frame]
            [reader.subs :as subs]
            [reader.upload.components :as upload-views]
            [reader.editor.components :as editor-views]))
