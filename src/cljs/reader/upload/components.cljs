(ns reader.upload.components
  (:require [re-frame.core :as re-frame]
            [ajax.core :as ajax]
            [reader.upload.api-data :as upload-api]))

(defn upload-files [e {:keys [id org-id]}]
  (js/setTimeout (fn []
                   (if-let [files-div (.getElementById js/document "file-input")]
                     (if-let [files (-> files-div
                                        .-files)]
                       (re-frame/dispatch [:reader.upload.events/upload-file
                                           (aget files 0)
                                           (.-name files-div)
                                           id
                                           org-id]))))
                 1000))

(defn upload-button []
  (let [dir-selected (re-frame/subscribe [:reader.directory.subs/selected-directory])]
    (if (:id @dir-selected)
      [:div [:div.btn {:on-click #(-> js/document
                                      (.getElementById "file-input")
                                      .click)}
             "Upload file"  [:i.icon [:i.fa.fa-upload]]]
       [:input {:type      "file"
                :id        "file-input"
                :on-change #(upload-files % @dir-selected)
                :name      "file-input"
                :multiple  false
                :style     {:margin-top "5px"
                            :padding    "10px"
                            :display    "none"}}]]
      [:div {:style {:font-size "18px"
                     :padding   "10px"}} "Please select a directory to upload files"])))
