(ns reader.upload.api-data
  (:require [ajax.core :as ajax]
            [re-frame.core :as re-frame]))

(defn upload-file [file name dir-id org-id]
  (let [form-data (doto (js/FormData.)
                    (.append name file)
                    (.append "directory" dir-id))]
    {:method          :post
     :uri             "/file-upload"
     :response-format (ajax/json-response-format {:keywords? true})
     :body            form-data
     :format          (ajax/json-request-format)
     :on-success      [:reader.upload.events/upload-file-success]
     :on-failure      [:reader.upload.events/upload-file-failure]}))
