(ns reader.upload.events
  (:require [re-frame.core :as re-frame]
            [reader.upload.api-data :as api-data]
            [day8.re-frame.http-fx]))

(re-frame/reg-event-fx
 ::upload-file
 (fn [{db :db} [_ file name dir-id org-id]]
   {:db         db
    :http-xhrio (api-data/upload-file file name dir-id org-id)}))

(re-frame/reg-event-db
 ::upload-file-success
 (fn [db [_ value]]
   db))


(re-frame/reg-event-db
 ::upload-file-failure
 (fn [db [_ value]]
   db))
