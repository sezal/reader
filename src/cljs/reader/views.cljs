(ns reader.views
  (:require
   [re-frame.core :as re-frame]
   [reader.subs :as subs]
   [reader.user.components :as user-views]
   [reader.organization.components :as org-views]
   [reader.panel.components :as panel]
   [reader.upload.components :as upload-views]
   [reader.header.components :as header-views]
   [reader.editor.components :as editor-views]
   [reader.directory.components :as dir-views]))

(defn main-panel []
  [:div
   [:div.topline]
   [:div.sidebar
    [:div.userinfo [(user-views/greeting)]]
    [:div.sidenav
     [dir-views/add-top-level-directory]
     [upload-views/upload-button]
     [:div.separator]
     [(org-views/organization-list)]]]
   [:div.main
    [:div.header  [header-views/directory-header]]
    [editor-views/editor]]])
