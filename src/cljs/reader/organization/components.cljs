(ns reader.organization.components
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [reader.organization.subs :as subs]
            [reader.organization.events :as events]
            [reader.directory.components :as directories]))

(defn organization-view [id name]
  "showing organization using directory view but with nil id, also initializing org-directories component"
  ^{:key id}
  [directories/directory-list id])


(defn organization-list []
  (let [org (re-frame/subscribe [::subs/organization])]
    (fn []
      [organization-view (:id @org) (:name @org)])))
