(ns reader.organization.api-data
  (:require [ajax.core :as ajax]
            [re-frame.core :as re-frame]
            [reader.db :as db]))

(enable-console-print!)

(defn get-org []
  {:method          :get
   :response-format (ajax/json-response-format {:keywords? true})
   :format          (ajax/json-request-format)
   :uri             "/orgs"
   :on-success      [:reader.organization.events/organizations-retrieved]})
