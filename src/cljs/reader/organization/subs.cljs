(ns reader.organization.subs
  (:require
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::organization
 (fn [db _]
   (get-in db [:selected :org])))
