(ns reader.organization.events
  (:require
   [re-frame.core :as re-frame]
   [day8.re-frame.http-fx]
   [reader.user.events]
   [reader.directory.api-data :as dir-api]))

(re-frame/reg-event-fx
 ::organizations-retrieved
 (fn [{db :db} [_ org-details]]
   (let [selected-org (select-keys (first org-details) [:id :name :slug])]
     {:http-xhrio (dir-api/get-directories (:id selected-org))
      :db         (-> db
                      (assoc-in [:selected :org] selected-org)
                      (assoc-in [:data :orgs (:id selected-org)] selected-org))})))
