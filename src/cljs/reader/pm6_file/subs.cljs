(ns reader.pm6-file.subs
  (:require [re-frame.core :as re-frame]))


(re-frame/reg-sub
 ::dir-files
 (fn [db [_ dir-id]]
   (get-in db [:data :files])))

(re-frame/reg-sub
 ::is-selected-file
 (fn [db [_ file-id]]
   (and file-id (= file-id (get-in db [:selected :org :directory :file :id])))))

(re-frame/reg-sub
 ::selected-file
 (fn [db [_]]
   (get-in db [:selected :org :directory :file])))

(re-frame/reg-sub
 ::selected-file-available-types
 (fn [db [_ file-id]]
   (get-in db [:form-data file-id :types])))
