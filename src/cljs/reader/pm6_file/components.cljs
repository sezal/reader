(ns reader.pm6-file.components
  (:require [re-frame.core :as re-frame]
            [reader.pm6-file.subs :as subs]))

(defn file [{:keys [id name location-directory] :as file} org-id]
  (let [selected (re-frame/subscribe [::subs/is-selected-file id])]
    [:span
     {:id       id
      :on-click #(re-frame/dispatch [:reader.pm6-file.events/file-selected file org-id location-directory])
      :style    {:color     (if @selected "yellow" "grey")
                 :font-size "small"}}
     [:div name]]))

(defn files-for-directory [org-id dir-id]
  (let [files (re-frame/subscribe [::subs/dir-files dir-id])]
    [:ul
     (for [f @files]
       ^{:key (:id f)} [:li [file f org-id]])]))
