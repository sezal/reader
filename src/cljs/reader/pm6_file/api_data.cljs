(ns reader.pm6-file.api-data
  (:require [re-frame.core :as re-frame]
            [ajax.core :as ajax]))

(defn get-files [org-id dir-id]
  {:method          :get
   :uri             (str "/orgs/" org-id  "/dirs/" dir-id "/files/")
   :response-format (ajax/json-response-format {:keywords? true})
   :on-success      [:reader.pm6-file.events/received-file-list]})


(defn get-file-types [org-id dir-id file-id]
  {:method          :get
   :uri             (str "/orgs/" org-id "/dirs/" dir-id "/files/" file-id "/get-types")
   :response-format (ajax/json-response-format {:keywords? true})
   :on-success      [:reader.pm6-file.events/received-file-types]})

(defn get-file-data-for-type [org-id dir-id file-id file-type]
  {:method          :get
   :uri             (str "/orgs/" org-id "/dirs/" dir-id "/files/" file-id "/file-data-for-type?file-type=" file-type)
   :response-format (ajax/json-response-format {:keywords? true})
   :on-success      [:reader.pm6-file.events/received-file]})

(defn request-file-translate [org-id dir-id file-id]
  {:method          :post
   :params          {}
   :uri             (str "/orgs/" org-id "/dirs/" dir-id "/files/" file-id "/translate-file")
   :format          (ajax/json-request-format)
   :response-format (ajax/json-response-format {:keywords? true})
   :on-success      [:reader.pm6-file.events/translate-requested-for-file]})
