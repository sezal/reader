(ns reader.pm6-file.events
  (:require [re-frame.core :as re-frame]
            [reader.pm6-file.api-data :as api-data]))

(re-frame/reg-event-db
 ::received-file-list
 (fn [db [_ files]]
   (assoc-in db [:data :files] files)))

(re-frame/reg-event-fx
 ::get-files-for-directory
 (fn [cofx [_ org-id dir-id]]
   {:http-xhrio (api-data/get-files org-id dir-id)}))

(re-frame/reg-event-fx
 ::file-selected
 (fn [{db :db} [_ file org-id dir-id]]
   {:db         (-> db
                    (assoc-in [:selected :org :directory :file] file)
                    (assoc :active-form (:id file)))
    :http-xhrio [(api-data/get-file-types org-id dir-id (:id file))
                 #_(api-data/get-eng-file org-id dir-id (:id file))]}))

(re-frame/reg-event-fx
 ::received-file-types
 (fn [{db :db} [_ types-with-location]]
   (let [file-id (:active-form db)]
     {:db       (assoc-in db [:form-data file-id :types] types-with-location)
      :dispatch [:reader.pm6-file.events/get-file-data-for-type file-id "text"]})))

(re-frame/reg-event-fx
 ::get-file-data-for-type
 (fn [{db :db} [_ file-id file-type]]
   (let [org-id (get-in db [:selected :org :id])
         dir-id (get-in db [:selected :org :directory :id])]
     {:db         db
      :http-xhrio (api-data/get-file-data-for-type org-id dir-id file-id "text")})))

(re-frame/reg-event-fx
 ::received-file
 (fn [{:keys [db]} [_ file]]
   {:db       db
    :dispatch [:reader.editor.events/text-form-changed (:file-id file) (:text file)]}))

#_(re-frame/reg-event-fx
   ::received-eng-file
   (fn [{:keys [db]} [_ file]]
     {:db       db
      :dispatch [:reader.editor.events/english-text-changed (:id file) (:text file)]}))

(re-frame/reg-event-fx
 ::file-translation-request
 (fn [{db :db} [_ file-id org-id dir-id]]
   {:db         db
    :http-xhrio [(api-data/request-file-translate org-id dir-id file-id)]}))
