(defproject reader "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.597"
                  :exclusions [com.google.javascript/closure-compiler-unshaded
                               org.clojure/google-closure-library
                               org.clojure/google-closure-library-third-party]]
                 [thheller/shadow-cljs "2.8.83"]
                 [reagent "0.9.1"]
                 [re-frame "0.11.0"]
                 ;;------------state setup
                 [mount "0.1.16"]
                 ;;-----------logging setup
                 [com.taoensso/timbre "4.10.0"]
                 ;;---------config setup
                 [aero "1.1.3"]
                 ;;---------- routes and server setup
                 [bidi "2.1.6"]
                 [ring/ring-jetty-adapter "1.8.0"]
                 [ring-oauth2 "0.1.4"]
                 [ring "1.7.0"]
                 [ring/ring-json "0.5.0"]
                 ;;----------validation
                 [prismatic/schema "1.1.12"]
                 ;;-----------postgres db setup
                 [ragtime "0.8.0"]
                 [org.clojure/java.jdbc "0.7.10"]
                 [org.postgresql/postgresql "42.2.8"]
                 ;;----------google bucket integration
                 [com.oscaro/clj-gcloud-storage "0.71-1.2"]
                 [com.oscaro/clj-gcloud-common "0.71-1.1"]
                 ;;----------job queue setup via rabbitmq
                 [org.clojure/core.async "1.1.587"]
                 [com.novemberain/langohr "5.1.0"]
                 ;;----------- cljs setup
                 [day8.re-frame/http-fx "0.1.6"]
                 [clj-commons/cljss "1.6.4"]
                 ;;------------ markdown editor
                 [markdown-to-hiccup "0.6.2"]
                 [markdown-clj "1.10.4"]
                 [hiccup "1.0.5"]
                 [hickory "0.7.1"]
                 ;;----------google translate
                 [com.google.cloud/google-cloud-translate "1.94.5"]
                 [com.google.guava/guava "22.0"]
                 ;;-------------------
                 [cheshire "5.10.0"]
                 [camel-snake-kebab "0.4.1"]
                 [medley "1.3.0"]]
  :plugins [[lein-shell "0.5.0"]]

  :main reader.core

  :min-lein-version "2.5.3"

  :source-paths ["src/clj" "src/cljs"]

  ;;:clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]


  :shell {:commands {"open" {:windows ["cmd" "/c" "start"]
                             :macosx  "open"
                             :linux   "xdg-open"}}}

  :aliases {"dev"          ["with-profile" "dev" "do"
                            ["run" "-m" "shadow.cljs.devtools.cli" "watch" "app"]]
            "prod"         ["with-profile" "prod" "do"
                            ["run" "-m" "shadow.cljs.devtools.cli" "release" "app"]]
            "build-report" ["with-profile" "prod" "do"
                            ["run" "-m" "shadow.cljs.devtools.cli" "run" "shadow.cljs.build-report" "app" "target/build-report.html"]
                            ["shell" "open" "target/build-report.html"]]
            "karma"        ["with-profile" "prod" "do"
                            ["run" "-m" "shadow.cljs.devtools.cli" "compile" "karma-test"]
                            ["shell" "karma" "start" "--single-run" "--reporters" "junit,dots"]]}

  :profiles
  {:dev
   {:dependencies [[binaryage/devtools "1.0.0"]
                   [clj-kondo "RELEASE"]]
    :source-paths ["dev"]}
   :uberjar {:aot         :all
             :main        reader.core
             :global-vars {*warn-on-reflection* true}
             :prep-tasks  ["compile" ["prod"]]}}

  :prep-tasks [])
